const fs = require('fs');
const path = require('path');
const glob = require("glob")

const PUBLIC_URL = `https://${process.env.VERCEL_URL}/`;
console.log({PUBLIC_URL})

if(process.env.NODE_ENV === 'production'){
  glob(__dirname+"/dist/*.js", function (err, files) {
    if(err){
      console.log(err)
      return;
    }
    console.log(files)
    files.forEach(replaceDepsUrls);
  })
}

function replaceDepsUrls(file){
  try {
    fs.readFile(file, 'utf8', (err, data)=>{
      if (err) {
        console.error(err)
        return
      }
      const editedData = data.replace(new RegExp(/\/static\//g), PUBLIC_URL);
      fs.writeFile(file, editedData, function (err) {
        if (err) return console.log(err);
        console.log(`File : ${file} is writenned`);
      });
    })
  } catch (err) {
    console.error(err)
  }
}