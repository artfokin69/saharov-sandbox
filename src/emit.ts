//TODO: хочется по имени эвента определять параметры
interface Event{
  eventName: string;
  cb: (...args: any[])=>void;
}


export default class Emit<Events extends string>{
  eventsCallbacks: Partial<Record<Events, ((...args: any[])=>void)[] | undefined>> = {};

  public on(event: Events, callback: (...args: any[])=>void){
    if(!this.eventsCallbacks[event]){
      this.eventsCallbacks[event] = [];
    }
    this.eventsCallbacks[event].push(callback)
  }
  public off(event: Events, callback){
    const callbacks = this.eventsCallbacks[event];
    if(callbacks){
      this.eventsCallbacks[event] = callbacks.filter(cb=>cb === callback)
    }
  }
  protected emit(event: Events, ...args: any[]){
    if(this.eventsCallbacks[event]){
      this.eventsCallbacks[event].forEach(cb=>cb.apply(null, args));
    }
  }
}