import autoBind from "auto-bind";
import { AdditiveBlending, BufferGeometry, Clock, Float32BufferAttribute, BufferAttribute, Int16BufferAttribute, Geometry, Mesh, Points, PointsMaterial, Scene, ShaderMaterial, Texture, Vector3, Color, Int32BufferAttribute, WebGLRenderTarget, LinearFilter, WebGLRenderer, PerspectiveCamera, FontLoader, MathUtils, BoxHelper, DoubleSide, InstancedBufferGeometry, InstancedBufferAttribute, Vector4, RawShaderMaterial } from "three";
import {VertexNormalsHelper} from 'three/examples/jsm/helpers/VertexNormalsHelper';
import { loadTexture } from "../helpers/loaders";
import snowTexture1 from '../../assets/snow/sf-1.png';
import snowTexture2 from '../../assets/snow/sf-2.png';
import particlesVertexShader2 from '../../assets/shaders/particles.vert';
import particlesFragmentShader2 from '../../assets/shaders/particles.frag';
import { GUI } from "dat.gui";
import Emit from "../emit";

const snowTexturePaths = [
  snowTexture1,
  snowTexture2,
]

const randomBetween = (min: number, max: number)=> min + Math.random() * (max - min);

interface ParticleGroup{
  id: number;
  speedLimits: [number, number];
  sizeLimits: [number, number];
  count: number;
}



class Particles extends Emit<'enabled'>{
  _enabled = true;
  particleGroups: ParticleGroup[] = [
    {
      id: 0,
      speedLimits: [6, 28],
      sizeLimits: [1,3],
      count: 40,
    },
    {
      id: 1,
      speedLimits: [9, 15],
      sizeLimits: [1, 7],
      count: 116
    }
  ]
  textures: Texture[];
  renderTarget: WebGLRenderTarget;
  mesh: Mesh<InstancedBufferGeometry, ShaderMaterial>;
  scene = new Scene();
  boxHelper: BoxHelper;
  get particlesCount(){
    return this.particleGroups.reduce((sum, g)=>sum+g.count,0);
  }
  set particlesCount(count: number){
    let intCount = Math.trunc(count);
    const percent = intCount / this.particlesCount;  
    let sum = 0;
    this.particleGroups.forEach((group,index)=>{
      if(index === this.particleGroups.length - 1){
        group.count = intCount - sum;
      } else{
        group.count = Math.trunc(group.count * percent);
      }
      sum += group.count;
    })
  }
  constructor(private renderer: WebGLRenderer, private camera: PerspectiveCamera){
    super();
    autoBind(this);
    const pixelRatio = window.devicePixelRatio;
    this.renderTarget = new WebGLRenderTarget(window.innerWidth * pixelRatio, window.innerHeight * pixelRatio, { minFilter: LinearFilter, magFilter: LinearFilter });
  }
  async load(){
    this.textures = await Promise.all(snowTexturePaths.map(loadTexture));
  }
  set enabled(value){
    this._enabled = value;
    this.emit('enabled', value);
  }
  get enabled(){
    return this._enabled;
  }
  
  init(){
    this.createParticles();
  }
  createParticles(){
    const geometry = new InstancedBufferGeometry();
   
    const vertices = [
      { pos: [-1, -1,  0], norm: [ 0,  0,  1], uv: [0, 0] },
      { pos: [ 1, -1,  0], norm: [ 0,  0,  1], uv: [1, 0] },
      { pos: [-1,  1,  0], norm: [ 0,  0,  1], uv: [0, 1] },
     
      // { pos: [-1,  1,  0], norm: [ 0,  0,  1], uv: [0, 1], },
      // { pos: [ 1, -1,  0], norm: [ 0,  0,  1], uv: [1, 0], },
      { pos: [ 1,  1,  0], norm: [ 0,  0,  1], uv: [1, 1] },
    ]
    geometry.setIndex([0,1,2,2,1,3])
    const geometryCenter = [0,0,0];
    const positions = vertices.map(v=>v.pos).flat()
    const UVs = vertices.map(v=>v.uv).flat()
    const offsets = [];
		const colors = [];
		const sizes = [];
    const speeds = [];
    const delays = [];
    this.particleGroups.forEach(group=>{
      for(let i = 0; i < group.count; i++){
        offsets.push( 
          randomBetween(-1,1),
          0,
          randomBetween(-1,1),
        );
  
        colors.push( 
          Math.random(), 
          Math.random(), 
          Math.random(), 
          Math.random() 
        );
  
        const size = randomBetween(group.sizeLimits[0], group.sizeLimits[1]);
        sizes.push(size);

        const speed = randomBetween(group.speedLimits[0], group.speedLimits[1]);
        speeds.push(speed);

        const delay = randomBetween(0, 10);
        delays.push(delay);
      }
    })
     
		geometry.instanceCount = this.particleGroups.reduce((sum,el)=>sum+el.count,0) // set so its initalized for dat.GUI, will be set in first draw otherwise

		geometry.setAttribute( 'position', new Float32BufferAttribute( positions, 3 ) );
		geometry.setAttribute( 'uv', new Float32BufferAttribute( UVs, 2 ) );
		geometry.setAttribute( 'offset', new InstancedBufferAttribute( new Float32Array( offsets ), 3 ) );
		geometry.setAttribute( 'scale', new InstancedBufferAttribute( new Float32Array( sizes ), 1 ) );
		geometry.setAttribute( 'speed', new InstancedBufferAttribute( new Float32Array( speeds ), 1 ) );
    geometry.setAttribute( 'delay', new InstancedBufferAttribute( new Float32Array( delays ), 1 ) );

    const material = new ShaderMaterial( {
      uniforms: {
        uTime: { value: 1.0 },
        uSineTime: { value: 1.0 },
        pointTextures: {value: this.textures},
        pointsCount: {value: snowTexturePaths.length},
        angle: {value: 0.5},
        skewY: {value: 0},
        skewX: {value: 0},
        scaleY: {value: 1},
        scaleX: {value: 1},
        rotateX: {value: 2.44},
        rotateY: {value: 2.44},
        sineTime: {value: 1},
        uParticlesOffsetX: {value: 500},
        uParticlesOffsetZ: {value: 1200},
        uCenter:{value: geometryCenter},
      },
      vertexShader: particlesVertexShader2,
      fragmentShader: particlesFragmentShader2,
      side: DoubleSide,
      transparent: true
    } );
   
    this.mesh = new Mesh( geometry, material );
    this.mesh.position.z = 500;
    this.mesh.position.y = 100;
    this.scene.add(this.mesh );
  }
  restartParticles(){
    this.scene.remove(this.mesh);
    this.createParticles();
  }
  public get texture(){
    return this.renderTarget?.texture || null;
  }
  
  onRender(clock: Clock){
    if(!this.enabled) return;
    this.renderer.setRenderTarget(this.renderTarget);
    this.renderer.clearColor();
    this.renderer.clearDepth();
    this.mesh.material.uniforms.uTime.value = clock.getElapsedTime();
		this.mesh.material.uniforms.uSineTime.value = Math.sin( this.mesh.material.uniforms.uTime.value * 0.05 );
    this.renderer.render(this.scene, this.camera);
    this.renderer.setRenderTarget(null); 
  }
  onResize(){
    const w = window.innerWidth;
    const h = window.innerHeight;
    const pixelRatio = this.renderer.getPixelRatio();
    this.renderTarget.setSize(w * pixelRatio, h * pixelRatio);
  }

  setupGui(gui: GUI){
    const folder = gui.addFolder('Particles');
    folder.add({'Enable': this.enabled}, 'Enable').onChange(value=>{
      this.enabled = value;
      if(value){
        this.createParticles();
      } else{
        this.scene.remove(this.mesh);
      }
    });
    const totalCountController = folder.add({'Count': this.particlesCount}, 'Count', 10, 1000, 1).onChange(value=>{
      this.particlesCount = value;
      this.restartParticles();
    })
    
    this.particleGroups.forEach((group,index)=>{
      const groupFolder = folder.addFolder(`Group ${index}`);
      groupFolder.add(group, 'count', 0, 500, 1).name(`Count`).listen().onChange(value=>{
        totalCountController.setValue(this.particlesCount);
      })
      groupFolder.add({'Min size': group.sizeLimits[0]}, 'Min size', 0, 20).onChange(value=>{
        group.sizeLimits[0] = value;
        this.restartParticles();
      })
      groupFolder.add({'Max size': group.sizeLimits[1]}, 'Max size', 1, 40).onChange(value=>{
        group.sizeLimits[1] = value;
        this.restartParticles();
      })
      groupFolder.add({'Min speed': group.speedLimits[0]}, 'Min speed', 0, 100).onChange(value=>{
        group.speedLimits[0] = value;
        this.restartParticles();
      })
      groupFolder.add({'Max speed': group.speedLimits[1]}, 'Max speed', 1, 100).onChange(value=>{
        group.speedLimits[1] = value;
        this.restartParticles();
      })
    })

  }
}

export default Particles;