import nprogress from 'nprogress';
import gsap from 'gsap';
import ScrollTrigger from 'gsap/ScrollTrigger'
import { disablePageScroll } from 'scroll-lock';
import DeviceDetector, {DeviceDetectorResult} from "device-detector-js";
import Webgl from './webgl';
import scroll from './scroll';
import {isMobile} from './helpers/media';
import './helpers/fixVh'
import { redirectNext } from './helpers/common';

console.info(
  `%cSaharov webgl, version: ${process.env.npm_package_version}`,
  "color: #0f9bd6; font-weight: bold;"
);


const DEVICE:DeviceDetectorResult = new DeviceDetector().parse(window.navigator.userAgent);
let osVersion;
try{
  osVersion = parseInt(DEVICE.os.version.match(/^([0-9]+)\./)[1]);
  const excludeIos = DEVICE.os.name === 'iOS' && osVersion <= 11;
  const excludeAndroid = DEVICE.os.name === 'Android' && osVersion <= 8
  if(excludeIos || excludeAndroid){
    redirectNext()
  }
} catch(err){
  console.log('os version not parsed:', DEVICE.os.version)
}


disablePageScroll();
scroll.disable();
const DEFAULT_FONT_SIZE = parseFloat(getComputedStyle(document.documentElement).fontSize);
scaleText();
window.addEventListener('resize', scaleText);

gsap.registerPlugin(ScrollTrigger);
scroll.proxy();

prepareContent();

let loadedCount = 0;
const loadedCountToRun = 2;
const loaded = ()=>{
  loadedCount++;
  if(loadedCount === loadedCountToRun){
    main()
  }
}

const previewVideoEl = document.querySelector('.appearance__bg video') as HTMLVideoElement;
if(previewVideoEl.readyState === 4){
  loaded()
} else {
  previewVideoEl.addEventListener('loadeddata', ()=>{
    if(previewVideoEl.readyState === 4){
      loaded();
    }
  })
}

window.addEventListener('load', main);

async function main(){
  nprogress.configure({ showSpinner: false, parent: '.appearance__progress', speed: 1000 });
  nprogress.start();

  const startTime = new Date();
  window.scrollTo(0,0);
  const webglContainer = document.getElementById('webglContainer') as  HTMLDivElement;
  const webglInstance = new Webgl(webglContainer);
  console.info('start show preloader')
  await showPreloader()
  console.info('end show preloader')
  await Promise.all([webglInstance.loadTexts(), webglInstance.load()]);
  await webglInstance.init();
  showEntryMessage(webglInstance);
  const endTime = new Date();
  console.log('Total load time: ', (endTime.getTime() - startTime.getTime()) / 1000);
}

function showPreloader(){
  let resolver;
  const promise = new Promise(r=>resolver = r);
  const quote = document.querySelector('.appearance__quote');
  const preloader = document.querySelector('#preloader') as HTMLElement;
  const appearanceBg = document.querySelector('.appearance__bg') as HTMLElement;
  const aftograph = document.querySelector('.appearance__aftograph');
  gsap.timeline({onComplete:resolver})
    .set(preloader, {display: 'none'})
    .from(appearanceBg, {opacity:0, duration: 0.5}, 0)
    .from('.appearance__loadMessage', {opacity: 0, duration: 0.5}, 0)
    .from('.appearance__progress', {opacity:0, duration:0.5}, 0.3)
    .from(quote, {opacity: 0, duration: 1.5}, .5)
    .from(aftograph, {opacity:0, duration: 1}, 1)

  return promise;
}

function hidePreloader(){
  const appearance = document.querySelector('.appearance') as HTMLElement;
  gsap.to(appearance, {
    delay: .3,
    opacity:0,
    duration: 1,
    onComplete:()=>{
      appearance.remove()
    }
  })
}

function showEntryMessage(webglInstance: Webgl){
  nprogress.set(0.99999);
  document.querySelector('.appearance__progress').classList.add('-loaded');
  const showTl = gsap.timeline()
    .to('.appearance__quoteWrapper', {autoAlpha: 0, duration: .5},0)
    .to('.appearance__loadMessage', {autoAlpha: 0, duration: .5},0)
    .to('.appearance__entry', {autoAlpha: 1, duration: .5},.5)
    .to('.appearance__entryBtnsWrapper', {opacity:1, duration: .5},.8)
  
  const enterBtn = document.querySelector('.appearance__entryBtn');
  const enterHandler = async ()=>{
    enterBtn.removeEventListener('click', enterHandler);  
    webglInstance.play()
    hidePreloader();
  }
  enterBtn.addEventListener('click', enterHandler);

  const skipBtn = document.querySelector('.appearance__skipBtn');
  skipBtn.addEventListener('click', redirectNext)
  
  
}

async function prepareContent(){
  const elsSelector = isMobile ? "[data-media='desktop']" : "[data-media='mobile']";
  const elsForRemove = document.querySelectorAll(elsSelector);
  elsForRemove.forEach(el=>el.remove()); 
}

function scaleText(){
  const originWinW = 1440;
  const originWinH = 870; 
  
  const winW = window.innerWidth;
  const winH = window.innerHeight;

  const [w, h] = getRenderedSize(winW, winH, originWinW, originWinH);
  const scaledSquare =  (w * h) / (originWinW * originWinH);
  let scaledFontSize =  (w / originWinW + h / originWinH) / 2 * DEFAULT_FONT_SIZE;
  document.documentElement.style.fontSize = scaledFontSize + 'px';
}

function getRenderedSize(currentWidth:number, currentHeight:number, originWidth:number, originHeight:number){
  const originRatio = originWidth / originHeight;
  const currentRatio = currentWidth / currentHeight;
  if(originRatio > currentRatio){
    return [
      currentWidth, 
      currentWidth / originRatio
    ]
  } else {
    return [
      currentHeight * originRatio,
      currentHeight
    ]
  }
}