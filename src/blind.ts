import ScrollTrigger from "gsap/ScrollTrigger";
import gsap from "gsap";
import Emit from "./emit";
import autoBind = require("auto-bind");
import scroll from "./scroll";
import { isTouch } from "./helpers/media";
import { redirectNext } from "./helpers/common";

class Blind extends Emit<'progress'>{
  blindStub: HTMLElement;
  contentContainer: HTMLElement;
  blindST: gsap.plugins.ScrollTriggerInstance;
  enterST: gsap.plugins.ScrollTriggerInstance;
  scrollToStartTL: gsap.core.Tween | gsap.core.Timeline;
  progressLimit = 0.4;
  // progressLimit = 0.99;
  scrollAcceleration = scroll.inertiaAcceleration;
  scorllDirection = 1;
  _isSnap = false;
  resizeTimer: number;
  speedAutoScrollToBottom = isTouch ? 2 : 5;
  speedAutoScrollToTop = isTouch ? 10 : 30;

  set isSnap(value: boolean){
    this._isSnap = value;
    if(this.isSnap){
      // scroll.disable();
      console.log("SNAP")
      scroll.animatedScroll = scroll.getScrollY();  //animated отображает скролл без инерции и спешит вперед инерции
      this.loop();
    } else {
      scroll.enable();
    }
  }
  get isSnap(){
    return this._isSnap;
  }

  get startScroll(){
    return this.blindST?.start || 0
  }
  get endScroll(){
    return this.blindST?.end || 0;
  }

  //если тач устройство - высчитывать snap когда мы отпустили палец(touchend)
  constructor(){
    super()
    autoBind(this);
    this.blindStub = document.getElementById('blindStub');
    this.contentContainer = document.getElementById('scrollableContent');
    this.hideBlindStub();
    this.createEnterST();
    this.createBlindST();
    this.addSnapListener();
  } 
  private createEnterST(){
    let timer;
    this.enterST = ScrollTrigger.create({
      trigger: '#scrollableContent',
      start: 'bottom-=1 bottom',
      onEnter:()=>{
        timer = setTimeout(this.showBlindStub, 1000);
      },
      onLeaveBack: ()=>{
        clearTimeout(timer);
        this.hideBlindStub();
      }
    })
  } 
  private createBlindST(){
    const blindAnimation = gsap.timeline({paused: true})
      .fromTo('#blind', {y: '100%'}, {y: '0%', ease: 'none', duration:1, onComplete:redirectNext}, 0)

    this.blindST = ScrollTrigger.create({
      trigger: '#blindStub',
      start: 'top bottom',
      end: "top 3%",
      onEnter:()=>{
        scroll.scrollSpeed = isTouch ? 1 : 2.25;
      },
      onUpdate:(self)=>{
        blindAnimation.totalProgress(self.progress);
        this.scorllDirection = self.direction;
        this.emit('progress', self.progress);
        let soundProgress = Math.max(0, self.progress - this.progressLimit) / (1 - this.progressLimit);
        window.soundController?.setVolume(1 - soundProgress);
      },
      onLeaveBack:()=>{
        scroll.scrollSpeed = 1;
        scroll.animatedScroll = scroll.getScrollY();
      } 
    })
  }
  private hideBlindStub(){
    this.blindStub.style.position = 'absolute';
    this.blindStub.style.bottom = '-1px';
    this.blindStub.style.height = '0';
  }
  private showBlindStub(){
    this.blindStub.style.position = 'relative';
    this.blindStub.style.bottom = '0';
    this.blindStub.style.height = '';  
    
    // this.hideContent();
  }

  private hideContent(){
    this.contentContainer.style.position = 'absolute';
    this.contentContainer.style.bottom = '101%';
  }
  private showContent(){
    this.contentContainer.style.position = '';
    this.contentContainer.style.bottom = '';
  }
  private addSnapListener(){
    scroll.on('deltaYChanged', (info)=>{
      if(this.blindST?.isActive){
        this.isSnap = false;
      }
    })
    let scrollTimer;
    if(isTouch){
      document.addEventListener('touchend', ()=>{
        clearTimeout(scrollTimer);
        if(this.blindST?.isActive){
          scrollTimer = setTimeout(()=>this.isSnap = true);
        }
      })
      document.addEventListener('touchstart', ()=>{
        clearTimeout(scrollTimer);
      })
    } else{
      scroll.on('deltaYChanged', (info)=>{
        clearTimeout(scrollTimer);
        if(this.blindST?.isActive){
          scrollTimer = setTimeout(()=>this.isSnap = true, 150);
        }
      })
    }
  }
  private loop(){
    if(!this.isSnap) return;
    const toEnd = this.blindST.progress > this.progressLimit;
    if(toEnd && scroll.scrollTop >= this.endScroll || !toEnd && scroll.scrollTop <= this.startScroll) return
    if(toEnd){
      scroll.scrollTop = Math.max(this.startScroll, Math.min(this.endScroll, scroll.scrollTop+this.speedAutoScrollToTop));
    } else {
      scroll.scrollTop = Math.max(this.startScroll, Math.min(this.endScroll, scroll.scrollTop-this.speedAutoScrollToBottom));
    }
    requestAnimationFrame(this.loop)
  }
  public onResize(){
    this.enterST?.kill()
    this.blindST?.kill()
    clearTimeout(this.resizeTimer)
    this.resizeTimer = setTimeout(()=>{
      this.createEnterST()
      this.createBlindST();
    }, 100)
  }
}

export default Blind;