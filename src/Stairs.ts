import autoBind from 'auto-bind';
import ScrollTrigger from "gsap/ScrollTrigger";
import gsap from 'gsap';
import {GUI} from 'dat.gui';
import { AnimationAction, AnimationClip, AnimationMixer, BackSide, Clock, Color, DoubleSide, FrontSide, Group, Mesh, MeshBasicMaterial, MeshPhongMaterial, PerspectiveCamera, PlaneBufferGeometry, PlaneGeometry, RGBAFormat, Scene, Vector3, WebGLRenderer, WebGLRenderTarget } from 'three';
import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import {loadGLTF} from './helpers/loaders';
import startsModelPath from '../assets/stairs-new.glb';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import Emit from './emit';
import {isMobile} from './helpers/media';
import scroll from './scroll';

export default class Stairs extends Emit<'appearanceComplete' | 'cameraStairsTrigger' | 'cameraUpdate'>{
  public camera = new PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 1000 );
  private scene = new Scene();
  private appScene: Scene;
  private renderer: WebGLRenderer;
  private appCamera: PerspectiveCamera;
  private gltf: GLTF;
  private meshGroup: Group;
  private mixer: AnimationMixer;
  private action: AnimationAction;
  private clip: AnimationClip;
  private loaded = false;
  private cameraCommonTl: gsap.core.Timeline;
  private cameraShowTl: gsap.core.Tween |  gsap.core.Timeline;
  private cameraScrollTl: gsap.core.Tween | gsap.core.Timeline;
  private cameraST: gsap.plugins.ScrollTriggerInstance;
  private lastYPosition: number;
  private resizeTimer: number;
  
  public get scrollEnabled(){
    return !!this.cameraST;
  }
  public get scrollProgress{
    return this.cameraST?.progress || 0;
  }

  constructor(renderer: WebGLRenderer, camera: PerspectiveCamera, scene: Scene){
    super();    
    this.appScene = scene;
    this.renderer = renderer;
    this.appCamera = camera; 
    autoBind(this)
  }
  public async load(){
    this.gltf = await loadGLTF(startsModelPath);
    console.log('stairs: ', this.gltf);
    this.loaded = true;
  }
  public init(){
    this.camera = this.gltf.cameras.find(c=>c.name === "Camera_f_A5") as THREE.PerspectiveCamera;
    this.meshGroup = this.gltf.scene;
    this.mixer = new AnimationMixer(this.meshGroup);
    this.clip = this.gltf.animations[0];
    this.action = this.mixer.clipAction(this.clip);
    this.action.play();
    this.fixMaterials(this.meshGroup)
    this.makeCameraTl();
    this.camera.position.set(0,1000,300)
    this.appScene.add(this.meshGroup);
  }
  public setupGui(gui: GUI, orbitControls: OrbitControls){
    const folder = gui.addFolder('Stairs')
    folder.add({['Scroll Animation']: !orbitControls.enabled}, 'Scroll Animation').onChange(enabled => {
      if(enabled){
        this.cameraST?.enable()
        orbitControls.enabled = false;
      } else {
        this.cameraST?.disable()
        orbitControls.enabled = true;
      }
    })
    // folder.add(this.backgroundPlane.position, 'z', 0, 240)
  }
  public playPrerender(){
    this.cameraShowTl.totalProgress(1).timeScale(5).reverse();
  }
  public playAppearance(){
    this.cameraShowTl.totalProgress(0).timeScale(1).play();
    this.cameraShowTl.eventCallback('onComplete', ()=>{
      this.mekaCameraST();
      this.emit('appearanceComplete');
    });
  }
  public cameraUpdate(){
    if(isMobile){
      this.camera.position.x = Math.max(-10, Math.min(this.camera.position.x, 2));
    }
  }
  public onBlindUpdate(progress: number){
    if(!this.lastYPosition){
      this.lastYPosition = this.camera.position.y;
    }
    if(progress === 0){
      // this.lastYPosition = 0
    }
    const shift = -5;
    //плохо что эти же параметры анимируются в таймлайне
    this.camera.position.y = this.lastYPosition + shift * progress;
  }
  private makeCameraTl(){
    const showDuration = 0.18;
    const finishTrigger = 1;
    const startZoomTimePercent = 0.6;
    const totalTime = {t:0};
    this.mixer.setTime(totalTime.t);
    const totalDuration = this.clip.duration * this.action.timeScale;
    this.cameraCommonTl = gsap.timeline({paused:true})
      .to(totalTime, {
        t:totalDuration, 
        duration:totalDuration, 
        ease:'none', 
        onUpdate:()=>{
          if(totalTime.t < totalDuration){
            this.mixer.setTime(totalTime.t);
          }
          const startMove = 0.6;
          if(this.cameraST && this.cameraST.progress > startMove && this.cameraST.progress < 1 && !isMobile){
            const yShift = -8;
            const xShift = 6;
            const localProgress = (this.cameraST.progress - startMove) / (1 - startMove);
            this.camera.position.y += yShift*localProgress;
            this.camera.position.x += xShift*localProgress;
          }
        }
      })
    this.cameraShowTl = this.cameraCommonTl.tweenFromTo(0,showDuration * totalDuration,{immediateRender: false, paused: true})
    const cameraScrolTween = this.cameraCommonTl.tweenFromTo(showDuration * totalDuration, totalDuration * finishTrigger,{immediateRender: false, ease: 'none'});
    const startZoomTime = startZoomTimePercent * totalDuration - showDuration * totalDuration;
    const duration = cameraScrolTween.totalDuration() - startZoomTime;
    
    this.cameraScrollTl = gsap.timeline({paused:true})
      .add(cameraScrolTween, '0')
      .fromTo(this.camera,{zoom:1}, {zoom: 1.7, duration, ease: 'none'}, `-=${duration}`)
  }
  private mekaCameraST(){
    this.cameraST = ScrollTrigger.create({
      trigger: '#scrollableContent',
      start: 'top top',
      end: "bottom bottom",
      onUpdate:(self)=>{
        this.cameraScrollTl.totalProgress(self.progress)
        this.emit('cameraUpdate', self.progress, this.camera)
      },
    })
  }
  private fixMaterials(group: Group){
    const stairs = group.getObjectByName('strairs_2') as Mesh;
    // stairs.material.fog = false;
    stairs.material.transparent = true;
    stairs.material.side = FrontSide;
    // stairs.material.needsUpdate = true;

    const walls = group.getObjectByName('walls') as Mesh;
    walls.material.transparent = true;
    walls.material.side = FrontSide;
    // walls.material.needsUpdate = true;

    const floor1 = group.getObjectByName('floor_1') as Mesh
    floor1.material.transparent = true;
    floor1.material.side = FrontSide;
    // floor1.material.needsUpdate = true;

    const floor2 = group.getObjectByName('floor_2') as Mesh
    floor2.material.transparent = true;
    floor2.material.side = DoubleSide;
    // floor2.material.needsUpdate = true;

    const backWall = group.getObjectByName('back_wall') as Mesh
    backWall.material.transparent = true;
    backWall.material.side = DoubleSide;
    // backWall.material.needsUpdate = true;

    group.traverse(obj=>{
      if(obj.material){
        obj.material.map && this.renderer.initTexture(obj.material.map)
        obj.material.envMap && this.renderer.initTexture(obj.material.envMap)
        obj.material.normalMap && this.renderer.initTexture(obj.material.normalMap)
        obj.material.alphaMap && this.renderer.initTexture(obj.material.alphaMap)
        obj.material.aoMap && this.renderer.initTexture(obj.material.aoMap)
        obj.material.bumpMap && this.renderer.initTexture(obj.material.bumpMap)
      }
    })
  }
  public onResize(){
    this.cameraST?.kill();
    clearTimeout(this.resizeTimer)
    this.resizeTimer = setTimeout(()=>{
      this.mekaCameraST();
    }, 100)
  }
  public onRender(clock: Clock){
  }
}
