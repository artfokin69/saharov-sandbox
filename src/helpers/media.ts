import DeviceDetector, {DeviceDetectorResult} from "device-detector-js";

const isMobileMQ = window.matchMedia("(max-width: 767px)");
isMobileMQ.addListener(e=>window.location.reload());
const DEVICE:DeviceDetectorResult = new DeviceDetector().parse(window.navigator.userAgent);

export const isTouch = ['smartphone', 'mobile', 'tablet'].includes(DEVICE.device.type) || !DEVICE.device.type;
export const isMobile = isMobileMQ.matches;