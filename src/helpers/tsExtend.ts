export function isWritable<T extends Object, K extends keyof T>(obj: T, key: K){
  const desc = 
      Object.getOwnPropertyDescriptor(obj, key) 
      || Object.getOwnPropertyDescriptor(Object.getPrototypeOf(obj), key)
      || {}
  return Boolean(desc.writable)
}