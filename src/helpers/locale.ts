const PROD_LOCALE = window.location.pathname == '/' ? 'ru' : 'en';
const DEV_LOCALE = document.documentElement.getAttribute('lang') === 'ru' ? 'ru' : 'en';
const LOCALE = process.env.NODE_ENV === 'production' ? PROD_LOCALE : DEV_LOCALE;
export const IS_RU_LOCALE = LOCALE === 'ru';

export default LOCALE;