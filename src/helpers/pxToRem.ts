export default function pxToRem(value: number){
  const DEFAULT_FONT_SIZE = 50;
  const scaleFactor = parseFloat(getComputedStyle(document.documentElement).fontSize) / DEFAULT_FONT_SIZE;
  return value * scaleFactor;
}