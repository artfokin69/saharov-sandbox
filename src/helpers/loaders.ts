import { GLTFLoader, GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import {TextureLoader, Texture, Object3D, Mesh, AudioLoader, WebGLRenderer, LoadingManager, Font, FontLoader} from 'three';
import bmfontLoader from 'load-bmfont';
import { KTX2Loader } from 'three/examples/jsm/loaders/KTX2Loader.js';
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader.js';
import { MeshoptDecoder } from 'three/examples/jsm/libs/meshopt_decoder.module.js';

const manager = new LoadingManager();
manager.onLoad = function ( ) {
	console.log( 'Loading complete!');
};
const fontLoader = new FontLoader();
const textureLoader = new TextureLoader();
const audioLoader = new AudioLoader();
const gltfLoader = new GLTFLoader(manager);
const dracoLoader = new DRACOLoader(manager);
dracoLoader.setDecoderPath('/decoder/');
gltfLoader.setDRACOLoader(dracoLoader);
gltfLoader.setMeshoptDecoder(MeshoptDecoder);


export function addKTX2Loader(renderer: WebGLRenderer){
  // const ktx2Loader = new KTX2Loader();
  // gltfLoader.setKTX2Loader( ktx2Loader.detectSupport(renderer));
}

export function loadGLTF(path): Promise<GLTF>{
  return new Promise((resolve,reject)=>{
    gltfLoader.load(path, function ( gltf ) {
      if(path === '/person_gltfpack.7e7f7a99.glb'){
        gltf.scene.traverse(el=>{
          if(el.material){  
            console.log(path, el.name, el.material)
          }
        });
      }
      resolve(gltf);
    }, function ( xhr ) {
      console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
    }, function ( error ) {
      console.error( error );
      reject();
    } );
  })
}
export function loadTexture(path): Promise<Texture>{
  return new Promise((resolve,reject)=>{
    textureLoader.load(path, function ( texture ) {
      resolve(texture);
    }, function ( xhr ) {
      console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
    }, function ( error ) {
      console.error( error );
      reject();
    } );
  })
}
export function loadAudio(path): Promise<AudioBuffer>{
  return new Promise((resolve,reject)=>{
    audioLoader.load(path, function ( buffer ) {
      resolve(buffer);
    }, function ( xhr ) {
      console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
    }, function ( error ) {
      console.error( error );
      reject();
    } );
  })
}

export function loadBmFont(path: string): Promise<any>{
  return new Promise((resolve,reject)=>{
    bmfontLoader(path, (err:any, font:any) => err ? reject(err) : resolve(font));
  })
}

export function loadFont(path: string): Promise<Font>{
  return new Promise((resolve,reject)=>{
    fontLoader.load(path, function ( font ) {
      resolve(font);
    }, function ( xhr ) {
      console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
    }, function ( error ) {
      console.error( error );
      reject();
    } );
  })
}

export function disposeDracoLoader(){
  // dracoLoader.dispose()
}