function scheduleMeasurement() {
  if (!performance.measureUserAgentSpecificMemory) {
    console.log("performance.measureUserAgentSpecificMemory() is not available.");
    return;
  }
  setTimeout(performMeasurement, 5000);
}

async function performMeasurement() {
  // 1. Invoke performance.measureUserAgentSpecificMemory().
  let result;
  try {
    result = await performance.measureUserAgentSpecificMemory();
  } catch (error) {
    if (error instanceof DOMException &&
        error.name === "SecurityError") {
      console.log("The context is not secure.");
      return;
    }
    // Rethrow other errors.
    throw error;
  }
  // 2. Record the result.
  console.log("Memory usage:", result);
  // 3. Schedule the next measurement.
  scheduleMeasurement();
}


export default performMeasurement;