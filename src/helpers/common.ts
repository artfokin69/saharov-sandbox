import { IS_RU_LOCALE } from "./locale"


export const redirectNext = ()=>{
  const prefix = IS_RU_LOCALE ? '/ru' : '/en'
  window.location.href = prefix+'/chapter-1';
}