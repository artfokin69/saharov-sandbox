import gsap from 'gsap'
import ScrollTrigger from 'gsap/ScrollTrigger'
import classes from 'extends-classes';
import TextGroup from './textGroupBase';
import Emit from '../emit';
import {isMobile, isTouch} from '../helpers/media';
import pxToRem from '../helpers/pxToRem';
import scrol from '../scroll';
import scroll from '../scroll';
import { IS_RU_LOCALE } from '../helpers/locale';
// interface TextGroupFinal extends TextGroup, Emit<'show'> {}

export default class TextGroupFinal extends TextGroup{
  scrollTriggers: gsap.plugins.ScrollTriggerInstance[];
  blindTL: gsap.core.Timeline | gsap.core.Tween;
  
  // todo: разобраться с ConstructorParameters с асбтрактным классом
  constructor(container: HTMLElement, renderRequest: ()=>void){
    super(container, renderRequest);
    this.addEventListeners();
  }

  public async createMeshes(...args: Parameters<TextGroup['createMeshes']>){
    super.createMeshes(...args);
    this.blindTL = IS_RU_LOCALE ? this.createRuBlindTL() : this.createEnBlindTL();
  }

  public removeScrollTriggers(){
    this.scrollTriggers.forEach(st=>st.kill());
  }

  public addScrollTriggers(){
    const trigger =  document.getElementById('scrollableContent')    
    const showTL = IS_RU_LOCALE ? this.getRuShowTL() : this.getEnShowTl();
    const showST = ScrollTrigger.create({
      // animation: showTL,
      trigger,
      start: 'bottom-=5 bottom',
      toggleActions: 'play none none reverse',
      onLeaveBack: ()=>{
        showTL.timeScale(5).reverse()
      },
      onEnter:()=>{
        // scrol.disable();
        showTL.timeScale(1).play();
      }
    })

    this.scrollTriggers = [
      showST,
    ]
  }

  private createEnBlindTL(){
    let prevProgress = 0;
    let isReversed = false;
    let tl:gsap.core.Timeline;
    tl = gsap.timeline({paused: true, onUpdate:()=>{
      const progress = tl.totalProgress()
      isReversed = progress < prevProgress;
      prevProgress = progress;
    }})
      
    if(isMobile){
      const firstLine = this.meshes[0];
      const secondLine = this.meshes[1];
      const thirdLine = this.meshes[2];
      tl
        .to(firstLine.mesh.position, {x:`+=${pxToRem(100)}`, y:`-=${pxToRem(170)}`, duration: 1, ease: 'none'}, 0)
        .to(secondLine.mesh.position, {x:`-=${pxToRem(100)}`, y:`-=${pxToRem(170)}`, duration: 1, ease: 'none'},0)
        .to(thirdLine.mesh.position, {x:`+=${pxToRem(100)}`, y:`-=${pxToRem(170)}`, duration: 1, ease: 'none'},0)
    } else {
      const firstLine = this.meshes[0];
      const secondLine = this.meshes[2];
      const leftDate = this.meshes[1];
      const rightDate = this.meshes[3];
      const thirdLine = this.meshes[4];
      tl
        .to(firstLine.mesh.position,  {x:`+=${pxToRem(50)}`, y:`-=${pxToRem(170)}`, duration: 1, ease:'none'},0)
        .to(leftDate.mesh.position,   {x:`-=${pxToRem(50)}`, y:`-=${pxToRem(170)}`, duration: 1, ease:'none'},0)
        .to(secondLine.mesh.position, {x:`-=${pxToRem(50)}`, y:`-=${pxToRem(170)}`, duration: 1, ease:'none'},0)
        .to(rightDate.mesh.position,  {x:`-=${pxToRem(50)}`, y:`-=${pxToRem(170)}`, duration: 1, ease:'none'},0)
        .to(thirdLine.mesh.position,  {x:`+=${pxToRem(50)}`, y:`-=${pxToRem(170)}`, duration: 1, ease:'none'},0)
    }  
    tl.add(()=>{
      const toOpacity = isReversed ? 1 : 0;
      gsap.to('#lastScreenDiscoverContent', {opacity:toOpacity, duration:0.5})
    }, 0.5)
    return tl;
  }

  private createRuBlindTL(){
    let prevProgress = 0;
    let isReversed = false;
    let tl:gsap.core.Timeline;
    tl = gsap.timeline({paused: true, onUpdate:()=>{
      const progress = tl.totalProgress()
      isReversed = progress < prevProgress;
      prevProgress = progress;
    }})

    if(isMobile){
      const firstLine = this.meshes[0];
      const secondLine = this.meshes[1];
      const thirdLine = this.meshes[2];
      const fourthLine = this.meshes[3];
      tl
        .to(firstLine.mesh.position, {x:`+=${pxToRem(100)}`, y:`-=${pxToRem(170)}`, duration: 1, ease: 'none'}, 0)
        .to(secondLine.mesh.position, {x:`-=${pxToRem(100)}`, y:`-=${pxToRem(170)}`, duration: 1, ease: 'none'},0)
        .to(thirdLine.mesh.position, {x:`+=${pxToRem(100)}`, y:`-=${pxToRem(170)}`, duration: 1, ease: 'none'},0)
        .to(fourthLine.mesh.position, {x:`-=${pxToRem(100)}`, y:`-=${pxToRem(170)}`, duration: 1, ease: 'none'},0)
    } else {
      const firstLine = this.meshes[0];
      const secondLine = this.meshes[2];
      const leftDate = this.meshes[1];
      const rightDate = this.meshes[3];
      const thirdLine = this.meshes[4];
      const fourthLine = this.meshes[5];
      tl
        .to(firstLine.mesh.position,  {x:`+=${pxToRem(50)}`, y:`-=${pxToRem(170)}`, duration: 1, ease:'none'},0)
        .to(leftDate.mesh.position,   {x:`-=${pxToRem(50)}`, y:`-=${pxToRem(170)}`, duration: 1, ease:'none'},0)
        .to(secondLine.mesh.position, {x:`-=${pxToRem(50)}`, y:`-=${pxToRem(170)}`, duration: 1, ease:'none'},0)
        .to(rightDate.mesh.position,  {x:`-=${pxToRem(50)}`, y:`-=${pxToRem(170)}`, duration: 1, ease:'none'},0)
        .to(thirdLine.mesh.position,  {x:`+=${pxToRem(50)}`, y:`-=${pxToRem(170)}`, duration: 1, ease:'none'},0)
        .to(fourthLine.mesh.position,  {x:`-=${pxToRem(50)}`, y:`-=${pxToRem(170)}`, duration: 1, ease:'none'},0)
    }  
    
    tl.add(()=>{
      const toOpacity = isReversed ? 1 : 0;
      gsap.to('#lastScreenDiscoverContent', {opacity:toOpacity, duration:0.5})
    }, 0.5)
    return tl;
  }

  public onBlindUpdate(progress: number){
    this.blindTL.totalProgress(progress);
  }
  
  private getRuShowTL(){
    const showTL = gsap.timeline({paused:true, onUpdate: this.renderReqeust, onComplete: ()=>{
      scroll.enable();
    }})
      
    if(isMobile){
      const firstLine = this.meshes[0];
      const secondLine = this.meshes[1];
      const thirdLine = this.meshes[2];
      const fourthLine = this.meshes[3];
      showTL
      .from(firstLine.mesh.position, {x:`-=${pxToRem(100)}`, duration: 1},0)
      .to(firstLine.mesh.material, {opacity: 1, duration: 1},0)
      .from(secondLine.mesh.position, {x:`+=${pxToRem(100)}`, duration: 1},0)
      .to(secondLine.mesh.material, {opacity: 1, duration: 1},0)
      .from(thirdLine.mesh.position, {x:`-=${pxToRem(100)}`, duration: 1},.3)
      .to(thirdLine.mesh.material, {opacity: 1, duration: 1},.3)
      .from(fourthLine.mesh.position, {x:`+=${pxToRem(100)}`, duration: 1},.3)
      .to(fourthLine.mesh.material, {opacity: 1, duration: 1},.3)
    } else {
      const firstLine = this.meshes[0];
      const secondLine = this.meshes[2];
      const leftDate = this.meshes[1];
      const rightDate = this.meshes[3];
      const thirdLine = this.meshes[4];
      const fourthLine = this.meshes[5];
      showTL
      .from(firstLine.mesh.position, {x:`-=${pxToRem(100)}`, duration: 1},0)
      .to(firstLine.mesh.material, {opacity: 1, duration: 1},0)
      .from(secondLine.mesh.position, {x:`+=${pxToRem(100)}`, duration: 1},0)
      .to(secondLine.mesh.material, {opacity: 1, duration: 1},0)

      .to(leftDate.mesh.material, {opacity: 1, duration: 1},.7)
      .to(rightDate.mesh.material, {opacity: 1, duration: 1},1)

      .from(thirdLine.mesh.position, {x:`-=${pxToRem(100)}`, duration: 1},.3)
      .to(thirdLine.mesh.material, {opacity: 1, duration: 1},.3)
      .from(fourthLine.mesh.position, {x:`+=${pxToRem(100)}`, duration: 1},.3)
      .to(fourthLine.mesh.material, {opacity: 1, duration: 1},.3)
    }  
    showTL.fromTo('#lastScreenDiscover', {autoAlpha:0, y: 20}, {autoAlpha:1, duration: 1.5, y:0}, '-=1')
    return showTL;
  }

  private getEnShowTl(){
    const showTL = gsap.timeline({paused:true, onUpdate: this.renderReqeust, onComplete: ()=>{
      scroll.enable();
    }})
      
    if(isMobile){
      const firstLine = this.meshes[0];
      const secondLine = this.meshes[1];
      const thirdLine = this.meshes[2];
      showTL
      .from(firstLine.mesh.position, {x:`-=${pxToRem(100)}`, duration: 1},0)
      .to(firstLine.mesh.material, {opacity: 1, duration: 1},0)
      .from(secondLine.mesh.position, {x:`+=${pxToRem(100)}`, duration: 1},0)
      .to(secondLine.mesh.material, {opacity: 1, duration: 1},0)
      .from(thirdLine.mesh.position, {x:`-=${pxToRem(100)}`, duration: 1},.3)
      .to(thirdLine.mesh.material, {opacity: 1, duration: 1},.3)
    } else {
      const firstLine = this.meshes[0];
      const secondLine = this.meshes[2];
      const leftDate = this.meshes[1];
      const rightDate = this.meshes[3];
      const thirdLine = this.meshes[4];
      showTL
      .from(firstLine.mesh.position, {x:`-=${pxToRem(100)}`, duration: 1},0)
      .to(firstLine.mesh.material, {opacity: 1, duration: 1},0)
      .from(secondLine.mesh.position, {x:`+=${pxToRem(100)}`, duration: 1},0)
      .to(secondLine.mesh.material, {opacity: 1, duration: 1},0)

      .to(leftDate.mesh.material, {opacity: 1, duration: 1},.7)
      .to(rightDate.mesh.material, {opacity: 1, duration: 1},1)

      .from(thirdLine.mesh.position, {x:`-=${pxToRem(100)}`, duration: 1},.3)
      .to(thirdLine.mesh.material, {opacity: 1, duration: 1},.3)
    }  
    showTL.fromTo('#lastScreenDiscover', {autoAlpha:0, y: 20}, {autoAlpha:1, duration: 1.5, y:0}, '-=1')
    return showTL;
  }

  private addEventListeners(){
    document.querySelector('#lastScreenDiscover').addEventListener('click', ()=>{
      console.log({
        maxScroll: scroll.maxScroll,
        currentScroll: scroll.getScrollY()
      })
      gsap.to(scroll, {animatedScroll: scroll.maxScroll, duration: 2, ease: 'power2.inOut'});
    })
  }

  private computeWindowBasedCoords(char: HTMLElement){
    let {top, left, right, bottom} = char.getBoundingClientRect()
    const fontSize = parseInt(getComputedStyle(char).fontSize);

    if(['история' , 'жизни', 'сахарова'].includes(char.textContent.toLowerCase())){
      top += fontSize*0.07;
      bottom -= fontSize*0.07;
    }
    if(char.textContent.toLowerCase() === 'андрея'){
      top += fontSize*0.07;
      bottom += fontSize*0.08;
    }
    left -= fontSize * 0.1;
    right += fontSize * 0.1;
    return {
      top,
      left,
      right,
      bottom,
    };
  }
}


// function applyMixins(derivedCtor: any, constructors: any[]) {
//   constructors.forEach((baseCtor) => {
//     Object.getOwnPropertyNames(baseCtor.prototype).forEach((name) => {
//       Object.defineProperty(
//         derivedCtor.prototype,
//         name,
//         Object.getOwnPropertyDescriptor(baseCtor.prototype, name) ||
//           Object.create(null)
//       );
//     });
//   });
// }