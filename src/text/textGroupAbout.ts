import TextGroup from './textGroupBase';
import gsap from 'gsap'
import ScrollTrigger from 'gsap/ScrollTrigger'

export default class TextGroupAbout extends TextGroup{
  scrollTriggers: gsap.plugins.ScrollTriggerInstance[];
  showST: gsap.plugins.ScrollTriggerInstance;

  public removeScrollTriggers(){
    this.scrollTriggers.forEach(st=>st.kill());
  }

  public addScrollTriggers(){
    this.scrollTriggers = [
      ...this.makeShowST(),
      this.makeHideST()
    ]
  }
  
  private makeShowST(){
    const scrollTriggers = this.meshes.map((mesh,index)=>{
      const showTL = gsap.fromTo(mesh.mesh.material, {opacity:0}, {opacity:1, paused: true, ease: 'none', immediateRender: false})
      const showST = ScrollTrigger.create({
        trigger: mesh.el,
        start: 'bottom 70%',
        end: 'bottom 50%',
        onUpdate(self){
          showTL.totalProgress(self.progress);
        }
      })
      return showST;
    }).flat()
    return scrollTriggers;
  }
  private makeHideST(){
    const trigger =  document.getElementById('scrollableContent')        
    const materials = this.meshes.map(mesh=>mesh.mesh.material);
    const hideTL = gsap.fromTo(materials,{opacity:1}, {opacity:0, duration: 1, paused: true, immediateRender: false});
    const hideST = ScrollTrigger.create({
      trigger,
      start: '90% bottom',
      end: 'bottom-=1 bottom',
      onUpdate: (self)=>{
        hideTL.totalProgress(self.progress);
      }
    })
    
    return hideST;
  }
}