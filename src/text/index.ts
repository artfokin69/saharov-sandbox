import { BufferGeometry, CanvasTexture, Color, DoubleSide, Fog, Font, Group, LinearFilter, Material, Matrix4, Mesh, MeshBasicMaterial, MeshStandardMaterial, Object3D, OrthographicCamera, PlaneBufferGeometry, PlaneGeometry, RGBFormat, Scene, TextGeometry, Vector2, WebGLRenderer, WebGLRenderTarget } from "three";
import ScrollTrigger from "gsap/ScrollTrigger";
import gsap from 'gsap';
import scroll from '../scroll';
import TextGroup from './textGroupBase';
import { Inertia } from "../helpers/inertia";
import { GUI } from "dat.gui";
import Emit from "../emit";
import TextGroupFirst from "./textGroupFirst";
import TextGroupAbout from "./textGroupAbout";
import TextGroupFinal from "./textGroupFinal";
import autoBind = require("auto-bind");

const GROUP_SELECTOR ='.js-text-group';

/*
  .js-text-group
    .js-textToChar -> compile to chars, for each char cut rectangle from canvas, then create mesh
      lorem ipsum
    .js-word -> cut rectangle from canvas, then create mesh
      lorem ipsum
  .js-text-group.js-textToChar
    lorem ipsum
  .js-text-group.js-word
    lorem ipsum
*/

class Text extends Emit<'lastTextShowChanged'>{
  scrollableContent: HTMLElement;
  fixedContentContainer: HTMLElement;
  appScene: Scene;
  appFog: Fog;
  renderer: WebGLRenderer;
  renderTarget: WebGLRenderTarget;
  camera: OrthographicCamera; 
  scene = new Scene();
  fixedGroupPlane = new Group();
  scrollableGroupPlane = new Group();
  textGroups: {
      first: TextGroupFirst,
      scroll: TextGroupAbout,
      last: TextGroupFinal,
  };
  scrollTriggers: gsap.plugins.ScrollTriggerInstance[] = [];
  font: Font;
  activeLoader = {loader: ()=>{}, cancel: ()=>{}}
  
  public get texture(){
    return this.renderTarget?.texture || null;
  }

  private get unsignedTextGroups(){
    return Object.values(this.textGroups);
  }

  constructor(renderer: WebGLRenderer, appScene: Scene, fog: Fog){
    super()
    autoBind(this);
    this.appFog = fog;
    this.scene.add(this.fixedGroupPlane);
    this.scene.add(this.scrollableGroupPlane);
    this.scrollableContent = document.getElementById('scrollableContent');
    this.fixedContentContainer = document.getElementById('fixedContentContainer');
    this.renderer = renderer;
    this.appScene = appScene;
    this.camera = new OrthographicCamera( 0, window.innerWidth, 0, window.innerHeight, 0, 1);
    const pixelRatio = window.devicePixelRatio;
    this.renderTarget = new WebGLRenderTarget(window.innerWidth * pixelRatio, window.innerHeight * pixelRatio, { minFilter: LinearFilter, magFilter: LinearFilter });
    
    this.textGroups = this.makeTextGroups();
    this.render();
    scroll.on('inertiaScroll', ({y, deltaY}) => this.onScroll(y, deltaY));
  }

  public async load(){
    const converted = await this.convertHTMLtoWebGL();
    if(converted){
      this.addScrollTriggers();
    }
  }

  public showFirstScreen(immediate = false){
    const firstGroup = this.unsignedTextGroups.find(group=>group instanceof TextGroupFirst) as TextGroupFirst;
    firstGroup?.show(immediate);
  }

  public onResize(){
    const w = window.innerWidth;
    const h = window.innerHeight;
    this.camera.right = w;
    this.camera.bottom = h;
    this.camera.updateProjectionMatrix();
    const pixelRatio = window.devicePixelRatio;
    this.renderTarget.setSize(w * pixelRatio, h * pixelRatio);
    this.removeScrollTriggers();
    scroll.disable();
    this.convertHTMLtoWebGL().then((converted)=>{
      if(converted){
        scroll.enable();
        this.addScrollTriggers();
        this.showFirstScreen(true);
      }
    });
  }

  public onRender(){
    this.render();
  }

  public onBlindUpdate(progress: number){
    (this.textGroups.last as TextGroupFinal)?.onBlindUpdate(progress);
  }

  public setupGui(gui: GUI){
  }

  private async convertHTMLtoWebGL(){
    this.activeLoader.cancel();
    const [loader, cancel, cancelState] = this.addCancelState();
    this.activeLoader = {
      loader,
      cancel
    }
    await loader();
    return !cancelState.canceled
  }

  private addCancelState(): [()=>void, ()=>void, {canceled: boolean}]{
    let cancelState = {canceled: false};
    const handler = this.convertHTMLtoWebGLHandler.bind(this, cancelState);
    const cancelFunction = ()=> cancelState.canceled = true;
    return [handler, cancelFunction, cancelState];
  }

  private async convertHTMLtoWebGLHandler(cancelState: {canceled: boolean}){
    await this.renderTextSynchronously(cancelState);
    this.render();
  }

  private async renderTextSynchronously(cancelState: {canceled: boolean}){
    for (let group of this.unsignedTextGroups){
      if(cancelState.canceled) break;
      await this.textGroupToMesh(group);
    }
  }

  private async renderTextAsync(){
    const promises = this.unsignedTextGroups.map(group=>this.textGroupToMesh(group));
    await Promise.all(promises)
  }

  private async textGroupToMesh(group: TextGroup){
    // group.container.style.opacity = '1';
    const canvas = await group.drawHTMLtoCanvas();
    const scenePlane = group.isFixed ? this.fixedGroupPlane : this.scrollableGroupPlane;
    group.createMeshes(canvas, scenePlane);
  }

  private makeTextGroups(){
    const firstSectionContainer = document.querySelector('.firstScreen .html2canvas') as HTMLElement;
    const aboutSectionContainer = document.querySelector('.aboutTextContainer .html2canvas') as HTMLElement;
    const lastSectionContainer = document.querySelector('.lastSection .html2canvas') as HTMLElement;

    return {
      first: new TextGroupFirst(firstSectionContainer, this.render),
      scroll: new TextGroupAbout(aboutSectionContainer, this.render),
      last: new TextGroupFinal(lastSectionContainer, this.render),
    }
      
  }

  private addScrollTriggers(){
    this.scrollTriggers = [
      ...this.scrollTriggers,
      this.makeFogST()
    ]
    Object.values(this.textGroups).forEach(group=>group.addScrollTriggers());
  }

  private removeScrollTriggers(){
    this.scrollTriggers.forEach(st => st.kill());
    Object.values(this.textGroups).forEach(group=>group.removeScrollTriggers());
    this.scrollTriggers = [];
  }

  private makeFogST(){
    const tl = gsap.fromTo(this.appFog, {near: 800, far: 1500}, {near: 0, far: 150, ease: 'Power3.out'});
    const st = ScrollTrigger.create({
      animation: tl,
      trigger: '#scrollableContent',
      start: '65% bottom',
      end: 'bottom bottom',
      scrub: true,
      onEnter:()=>{
        this.appScene.fog = this.appFog;
      },
      onLeaveBack:()=>{
        this.appScene.fog = null;
      }
    })
    return st;
  }

  private onScroll(scrollTop: number, delta: number){
    if(delta !== 0){
      this.scrollableGroupPlane.position.y = -scrollTop;
      this.render();
    }
  }

  private render(){
    requestAnimationFrame(()=>{
      this.renderer.setRenderTarget(this.renderTarget);
      this.renderer.clearColor();
      this.renderer.clearDepth();
      this.renderer.render(this.scene, this.camera);
      this.renderer.setRenderTarget(null);
    })
  }
}


export default Text;