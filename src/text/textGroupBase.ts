import html2canvas from "html2canvas";
import SplitType from "split-type";
import { CanvasTexture, DoubleSide, Group, LinearFilter, Material, Mesh, MeshBasicMaterial, OrthographicCamera, PlaneBufferGeometry, Scene, Vector2, WebGLRenderer, WebGLRenderTarget } from "three";
import gsap from 'gsap';
import autoBind = require("auto-bind");
import scroll from '../scroll';

interface Coords{
  top: number;
  right: number;
  bottom: number;
  left: number;
}

const CHARS_CONTAINER_SELECTOR = '.js-textToChar';
const WORDS_CONTAINER_SELECTOR = '.js-solidText';

/* 
  If you wish to exclude certain Elements from getting rendered, 
  you can add a data-html2canvas-ignore attribute to those elements and html2canvas will exclude them from the rendering.
*/

export default abstract class TextGroup{
  container: HTMLElement;
  separatedElements: HTMLElement[] = [];
  meshes: {mesh: Mesh, el: HTMLElement}[] = [];
  isFixed: boolean;
  charOffsetX = 0.03;
  charOffsetY = 0.06;
  fixedFullScreenElement: HTMLElement;
  renderReqeust: ()=>void;
  
  public get id(){
    return this.container.id || "";
  }

  constructor(container: HTMLElement, renderRequest: ()=>void){
    autoBind(this);
    this.container = container;
    this.renderReqeust = renderRequest;
    this.fixedFullScreenElement = document.getElementById('fixedFullScreenElement');
    this.separatedElements = this.makeSeparatedElements();
    this.isFixed = this.container.hasAttribute('data-fixed');
  }
  
  public abstract addScrollTriggers(): void;
  public abstract removeScrollTriggers(): void;

  public async drawHTMLtoCanvas(){
    const canvas = await html2canvas(this.container, {
      backgroundColor: null,
      scrollX: 0,
      scrollY: 0,
      allowTaint: true,
      useCORS: true,
      // windowWidth: window.innerWidth,
      // windowHeight: this.fixedFullScreenElement.offsetHeight
    }); 
    return canvas;
  }

  public async createMeshes(canvas: HTMLCanvasElement, scene: Scene | Group){    
    this.meshes.forEach(mesh=>scene.remove(mesh.mesh));
    const startDate = new Date();
    this.meshes = this.separatedElements.map(char=>{
      const mesh = this.makeMesh(char, canvas)
      this.addToScene(mesh, scene, char);
      return {
        el: char,
        mesh,
      }
    });    
    const endDate = new Date();
    // console.log('Compile mesh: ', (endDate.getTime() - startDate.getTime()) / 1000)
  }

  public hideChars(){
    this.separatedElements.forEach(el=>el.style.opacity = '0')
  }

  public showChars(){
    this.separatedElements.forEach(el=>el.style.opacity = '1')
  }

  private makeMesh(char: HTMLElement, canvas: HTMLCanvasElement){
    const charCoords = this.computeWindowBasedCoords(char);
    const canvasBasedCoords = this.computeCanvasBasedCoords(charCoords);
    let charCanvas = this.cropCanvas(canvas, canvasBasedCoords);
    const texture = new CanvasTexture(charCanvas);
    const width = charCoords.right - charCoords.left;
    const height = charCoords.bottom - charCoords.top;
    // console.log(char.textContent,{
    //   width,
    //   height,
    //   charCoords,
    //   canvasBasedCoords,
    //   originTop: char.getBoundingClientRect().top,
    //   originLeft: char.getBoundingClientRect().left,
    //   originWidth: char.getBoundingClientRect().width,
    //   originHeight: char.getBoundingClientRect().height,
    // });
    const geometry = new PlaneBufferGeometry(width, height);
    const material = new MeshBasicMaterial({
      map: texture,
      transparent: true,
      opacity: 0,
    });
    material.map.minFilter = LinearFilter;
    const mesh = new Mesh(geometry, material);
    mesh.name = char.id || 'mesh';
    return mesh;
  }

  private addToScene(mesh: Mesh, scene: Scene | Group, char: HTMLElement){
    const charCoords = this.computeWindowBasedCoords(char);
    const width = charCoords.right - charCoords.left;
    const height = charCoords.bottom - charCoords.top;    
    mesh.position.x = charCoords.left + width / 2;
    mesh.position.y = charCoords.top + height / 2;
    mesh.rotateX(Math.PI);
    scene.add(mesh);
  }

  private computeWindowBasedCoords(char: HTMLElement){
    let {top, left, right, bottom} = char.getBoundingClientRect()

    const fontSize = parseInt(getComputedStyle(char).fontSize);
    const offsetX = fontSize * this.charOffsetX;
    const offsetY = fontSize * this.charOffsetY;
    top -= offsetY;
    // bottom += offsetY;
    left -= offsetX;
    right += offsetX;
    
    if([...char.textContent].find(char=>['Й'].includes(char))){
     top -= offsetY * 5; 
    }
    if([...char.textContent].find(char=>['й'].includes(char))){
      top -= offsetY; 
    }
    if([...char.textContent].find(char=>['д'].includes(char.toLowerCase()))){
      bottom += offsetY;
    }
    if(['сахаров', 'сахарова'].includes(char.textContent.trim().toLowerCase())){
      top += offsetY;
    }
    
    return {
      top,
      left,
      right,
      bottom,
    };
  }

  private computeCanvasBasedCoords(charWindowBasedCoords: Coords){
    const containerCoords = this.container.getBoundingClientRect();
    const scaleFactor = window.devicePixelRatio || 1;
    const localCoords: Coords = {
      top: (charWindowBasedCoords.top - containerCoords.top) * scaleFactor,
      left: (charWindowBasedCoords.left - containerCoords.left) * scaleFactor,
      right: (charWindowBasedCoords.right - containerCoords.left) * scaleFactor,
      bottom: (charWindowBasedCoords.bottom - containerCoords.top) * scaleFactor,
    }
    return localCoords;
  }

  private cropCanvas(canvas: HTMLCanvasElement, coords: Coords) {
    const ctx = canvas.getContext('2d');
    const imageData = ctx.getImageData(coords.left , coords.top, coords.right, coords.bottom);
    const newCan = document.createElement('canvas');
    newCan.width = coords.right - coords.left;
    newCan.height = coords.bottom - coords.top;
    const newCtx = newCan.getContext('2d');
    newCtx.putImageData(imageData, 0, 0);
    return newCan;    
  }
  
  private makeSeparatedElements(){
    const containerRequireBeSeparateAlone = this.container.classList.contains(WORDS_CONTAINER_SELECTOR.substr(1));
    if(containerRequireBeSeparateAlone){
      return [this.container]
    }
    let containers = [];
    const containerRequireBeSeparateWithChars = this.container.classList.contains(CHARS_CONTAINER_SELECTOR.substr(1));
    if(containerRequireBeSeparateWithChars){
      containers = [this.container];
    } else {
      containers = [...this.container.querySelectorAll(`${CHARS_CONTAINER_SELECTOR}, ${WORDS_CONTAINER_SELECTOR}`)];
    }
    return containers.map(container=>{
      const splitCharsRequired = container.classList.contains(CHARS_CONTAINER_SELECTOR.substr(1));
      if(splitCharsRequired){
        return new SplitType(container, { types: 'lines' }).lines
      } else {
        return container;
      }
    }).flat()
  }
}