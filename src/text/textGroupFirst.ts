import TextGroup from './textGroupBase';
import gsap from 'gsap'
import ScrollTrigger from 'gsap/ScrollTrigger'
import pxToRem from '../helpers/pxToRem';

export default class TextGroupFirst extends TextGroup{
  scrollTriggers: gsap.plugins.ScrollTriggerInstance[];

  public show(immediate: boolean){
    const tl = gsap.timeline()
      .delay(1.5)
      .from(this.meshes[0].mesh.position, {x: `+=${pxToRem(200)}`, duration: 1}, 0)
      .from(this.meshes[1].mesh.position, {x: `-=${pxToRem(200)}`, duration: 1}, 0)
      .to(this.meshes[0].mesh.material, {opacity:1, duration: 1}, 0)
      .to(this.meshes[1].mesh.material, {opacity:1, duration: 1}, 0)
      .to(this.meshes[2].mesh.material, {opacity:1, duration: .5}, .5)
      .addLabel('showAdditionalContent', '-=.1')
      .to('#firstScreenScrollDown', {opacity: 1, duration: .5}, 'showAdditionalContent')
      .to('.coins__images', {opacity: 1, duration: .5}, 'showAdditionalContent')
      .to('.coinsTitle', {opacity: 1, duration: .5}, 'showAdditionalContent')


    if(immediate){
      tl.totalProgress(1);
    }  
    return tl;
  }

  public removeScrollTriggers(){
    this.scrollTriggers.forEach(st=>st.kill());
    this.scrollTriggers = [];
  }

  public addScrollTriggers(){
    this.scrollTriggers = [
      this.getFadeST(),
      this.getHideBtnST(),
    ]
  }

  private getFadeST(){
    const tl = gsap.timeline({paused: true})
    .to(this.meshes[0].mesh.position, {x: `-=${pxToRem(50)}`, y: `-=${pxToRem(50)}`, ease: 'none'}, 0)
    .to(this.meshes[1].mesh.position, {x: `+=${pxToRem(50)}`, y: `-=${pxToRem(50)}`, ease: 'none'}, 0)
    .to(this.meshes[2].mesh.position, {y: `-=${pxToRem(50)}`, ease: 'none'}, 0)
    .to(this.meshes[0].mesh.material, {opacity:0}, '.01')
    .to(this.meshes[1].mesh.material, {opacity:0}, '.01')
    .to(this.meshes[2].mesh.material, {opacity:0}, '.01')
    

    const hideST = ScrollTrigger.create({
      animation: tl,
      trigger: document.body,
      start: 'top top',
      end: '+=50%',
      scrub: true,
    })
    return hideST;
  }
  private getHideBtnST(){
    const scrollBtn = document.getElementById('firstScreenScrollDownContent');
    const tl = gsap.to(scrollBtn, {opacity:0})
    const st = ScrollTrigger.create({
      animation: tl,
      trigger: scrollBtn,
      start: 'bottom bottom',
      end: '+=100',
      scrub: true,
    })
    return st;
  }
}