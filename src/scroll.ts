import ScrollTrigger from "gsap/ScrollTrigger";
import ResizeSensor from 'resize-sensor';
import Emit from "./emit";
import { Inertia } from "./helpers/inertia";
import autoBind = require("auto-bind");
import DeviceDetector, {DeviceDetectorResult} from "device-detector-js";
import VirtualScroll from 'virtual-scroll';

const DEVICE:DeviceDetectorResult = new DeviceDetector().parse(window.navigator.userAgent);

interface ScrollInfo{
  yNative: number;
  y: number;
  deltaY: number;
  deltaYNative: number;
}


class Scroll extends Emit<'nativeScroll' | 'inertiaScroll' | 'deltaYChanged'>{
  private _scrollTop = 0;
  private deltaLimit = 100;
  public disabled = false;
  private scrollInertia = new Inertia(0, 0, 0.25, 0.25, 0);
  private virtualScroll: VirtualScroll;
  private oldScrollTop = 0
  private oldInertiaScrollTop = 0;

  public scrollSpeed = 1;
  public useInertia = true;

  private get scrollInfo(): ScrollInfo{
    return {
      yNative: this.scrollTop,
      y: this.inertiaScrollTop,
      deltaY: this.deltaY,
      deltaYNative: this.deltaYNative,
    }
  }

  public set scrollTop(value){
    this.oldScrollTop = this.scrollTop;
    const roundedValue = this.round(value);
    this._scrollTop = parseFloat(roundedValue.toFixed(3));
    this.emit('nativeScroll', this.scrollInfo);
  }
  
  public get scrollTop(){
    return this._scrollTop;
  } 

  private set inertiaScrollTop(value: number){
    this.oldInertiaScrollTop = this.inertiaScrollTop;
    const rounededValue = parseFloat(this.round(value).toFixed(3));
    if(this.useInertia){
      this.scrollInertia.update(rounededValue);
    } else {
      this.scrollInertia.setValue(rounededValue);
    }
    ScrollTrigger.update();
    this.emit('inertiaScroll', this.scrollInfo);
  }
  
  private get inertiaScrollTop(){
    return parseFloat(this.scrollInertia.value.toFixed(1));
  } 

  public set maxScroll(value: number){
    this.scrollInertia.maxV = value;
  }
  
  public get maxScroll(){
    return this.scrollInertia.maxV;
  }

  private get deltaYNative(){
    return this.scrollTop - this.oldScrollTop;
  }
  
  private get deltaY(){
    return this.inertiaScrollTop - this.oldInertiaScrollTop;
  }

  public set inertiaAcceleration(value: number){
    this.scrollInertia.acc = value
  }

  public get inertiaAcceleration(){
    return this.scrollInertia.acc;
  }

  public set inertiaFriction(value: number){
    this.scrollInertia.friction = value
  }

  public get inertiaFriction(){
    return this.scrollInertia.friction;
  }

  public set animatedScroll(value: number){
    const useInertia = this.useInertia;
    this.useInertia = false;
    this.scrollTop = value;
    this.inertiaScrollTop = value;
    this.useInertia = useInertia;
  }
  public get animatedScroll(){
    return this.scrollTop;
  }

  constructor(){
    super();
    autoBind(this);
    this.setMaxScroll();
    this.addListeners();
    this.addInertiaHelper();
  }
  
  public proxy(inertia = true){
    ScrollTrigger.scrollerProxy(document.body, {
      scrollTop: (value) => {
        // для snap
        if (value !== undefined) {
          console.log('SET POSITION', {
            value,
            max: this.maxScroll
          });
          this.animatedScroll = value;
        }
        const newValue = inertia ? this.inertiaScrollTop : this.scrollTop
        return parseFloat(newValue.toFixed(1))
      },
      getBoundingClientRect() {
        return {top: 0, left: 0, width: window.innerWidth, height: window.innerHeight};
      }
    })
  }
  public update(){
    this.scrollTop = this.scrollTop;
  }
  public disable(){
    this.disabled = true;
  }
  public enable(){
    this.disabled = false;
  }
  public getScrollY(){
    return this.inertiaScrollTop
  }
  public getScrollYNative(){
    return this.scrollTop;
  }
  public getProgress(){
    return this.getScrollY() / this.maxScroll;
  }
  private round(value){
    const maxScrollTop = document.body.offsetHeight - window.innerHeight;
    return Math.max(0, Math.min(value, maxScrollTop));
  }
  private addScroll(delta: number){
    delta = Math.min(this.deltaLimit, Math.abs(delta)) * Math.sign(delta);
    delta *= this.scrollSpeed
    if(!this.disabled){
      this.scrollTop += delta;
    }
    this.emit('deltaYChanged', {delta});
  }
  private addListeners(){
    this.virtualScroll = new VirtualScroll({
      el: document,
      passive: true,
      touchMultiplier: 1,
      mouseMultiplier: 0.3,
      useKeyboard: true
    })
    this.virtualScroll.on((event)=>{
      this.addScroll(-event.deltaY)
    })

    new ResizeSensor(document.body, this.setMaxScroll);
    window.addEventListener('resize', ()=>{
      this.animatedScroll = 0;
      this.setMaxScroll();
      this.update();
    })
  }
  private setMaxScroll(){
    this.maxScroll = document.body.offsetHeight - window.innerHeight;
  }
  private addInertiaHelper(){
    window.requestAnimationFrame(this.inertiaLoopHandler);
  }
  private inertiaLoopHandler(){
    if(this.inertiaScrollTop !== this.scrollTop){
      this.inertiaScrollTop = this.scrollTop;
    }
    window.requestAnimationFrame(this.inertiaLoopHandler);
  }
}

const scroll = new Scroll();
export default scroll;