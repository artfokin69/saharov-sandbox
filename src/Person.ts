import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import {AdditiveBlending, AnimationAction, AnimationClip, AnimationMixer, BufferGeometry, ClampToEdgeWrapping, Clock, Color, CubeReflectionMapping, CubeRefractionMapping, CubeUVReflectionMapping, CubeUVRefractionMapping, DoubleSide, EquirectangularReflectionMapping, EquirectangularRefractionMapping, Group, LoopPingPong, MathUtils, Mesh, MeshBasicMaterial, MeshPhongMaterial, MeshStandardMaterial, MirroredRepeatWrapping, MixOperation, MultiplyBlending, NormalBlending, RepeatWrapping, Scene, SkinnedMesh, sRGBEncoding, SubtractiveBlending, Texture, Vector2, MultiplyOperation, AddOperation, WebGLRenderer, BoxBufferGeometry, SpotLight} from 'three';
import autoBind from 'auto-bind';
import gsap from 'gsap';
import ScrollTrigger from 'gsap/ScrollTrigger';
import {GUI} from 'dat.gui';
import personModelPath from '../assets/person_c_textures_gltfpack_latest.glb';
import {loadGLTF, loadTexture} from './helpers/loaders';
import  { DegRadHelper } from './helpers/gui';
import eyeEnvMapTexture from '../assets/eye-env.jpg';
import eyeEnvMapTexture2 from '../assets/eye-envmap2.png';
import glassesEnvMap from '../assets/glasses-env.png';
import eyeEnvMapTexture from '../assets/eye-env.jpg';
import eyeEnvMapTexture2 from '../assets/eye-envmap2.png';
import glassesEnvMap from '../assets/glasses-env.png';
import { isWritable } from './helpers/tsExtend';
import cloneDeep from 'lodash.clonedeep';


export default class Person{
  renderer: WebGLRenderer;
  scene: Scene;
  gltf: GLTF;
  head: SkinnedMesh<BufferGeometry, MeshStandardMaterial>;
  coat: SkinnedMesh<BufferGeometry, MeshStandardMaterial>;
  hatMaterial: MeshStandardMaterial;
  eye: SkinnedMesh<BufferGeometry, MeshBasicMaterial>;
  meshGroup: Group;
  paltoMesh: Mesh;
  clip: AnimationClip;
  mixer: AnimationMixer;
  action: AnimationAction;
  loaded = false;
  isInit = false;
  turnRequired = false;
  turnTl: gsap.core.Tween;
  turnST: gsap.plugins.ScrollTriggerInstance;
  loopedTl: gsap.core.Tween;
  appearTl: gsap.core.Tween;
  finalLoopedTl: gsap.core.Tween;
  tweens: gsap.core.Tween[];
  eyeEnvMap: Texture;
  eyeGlossMaterial: MeshPhongMaterial;
  legsMaterial: MeshStandardMaterial;
  eyeSpotLight: SpotLight;
  resizeTimer: number;

  get activeTween(){
    return this.tweens.find(tween=>tween.isActive());
  }
  constructor(renderer: WebGLRenderer, scene: Scene){
    this.renderer = renderer;
    this.scene = scene;
    autoBind(this);
  }
  
  public async load(){
    const [gltf, eyeEnvMap] = await Promise.all([loadGLTF(personModelPath), loadTexture(eyeEnvMapTexture)]);
    this.gltf = gltf;
    this.eyeEnvMap = eyeEnvMap;
    eyeEnvMap.mapping = EquirectangularReflectionMapping;
    eyeEnvMap.encoding = sRGBEncoding;
    eyeEnvMap.repeat.set(1,1);
    eyeEnvMap.wrapS = ClampToEdgeWrapping;
    eyeEnvMap.wrapT = ClampToEdgeWrapping;
    
    console.log('person: ', this.gltf);
    this.gltf.scene.traverse(el=>{
      if(el.name === 'GEO_Eye_Base_R' || el.name === "GEO_Eye_Gloss_R" || el.name === 'GEO_Eye_Base_R' || el.name === "GEO_Eye_Gloss_R"){
        el.traverse(child=>{
          child.material?.polygonOffset = true;
          child.material?.polygonOffsetFactor = 3.1;
          child.material?.polygonOffsetUnit = 3.1;
        })
      }
      if(el.name === "GEO_Sakharov_Head"){
        
      }   
      if(el.material?.name === "M_Sakharov_Hat_V2"){
        this.hatMaterial = el.material;
        this.hatMaterial.polygonOffset = true;
        this.hatMaterial.polygonOffsetFactor = -1;
        this.hatMaterial.polygonOffsetUnits = -1;
        this.hatMaterial.side = DoubleSide;
        // this.hatMaterial.visible = false;
        // el.material?.transparent = true;
        // el.material?.opacity = 1;
        // el.traverse(child=>{
        //   child.material?.transparent = true;
        //   child.material?.opacity = 1;
        // })
      }   
      if(el.material?.name === "M_Sakharov_Legs"){
        this.legsMaterial = el.material;
        this.legsMaterial.polygonOffset = true;
        this.legsMaterial.polygonOffsetFactor = -1;
        this.legsMaterial.polygonOffsetUnits = -1;
        // el.material?.polygonOffset = true;
        // el.material?.polygonOffsetFactor = 1.0;
        // el.material?.polygonOffsetUnit = 4.0
        // el.traverse(child=>{
        //   child.material?.polygonOffset = true;
        //   child.material?.polygonOffsetFactor = 1.0;
        //   child.material?.polygonOffsetUnit = 4.0
        // })
      }
      if(el.material?.name === "M_Sakharov_Head"){
        this.head = el as SkinnedMesh<BufferGeometry, MeshStandardMaterial>;
        this.head.material.normalScale = new Vector2(-1,-1);
        // this.head.material.visible = false;
      }
    
      if(el.material?.name === 'M_Sakharov_Coat'){
        console.log('coat: ', el.material)
        const material = (el.material as MeshStandardMaterial);
        this.coat = el as SkinnedMesh<BufferGeometry, MeshStandardMaterial>;
        const newMaterial = new MeshPhongMaterial();
        const propertiesToCopy: 
        (
          (keyof MeshStandardMaterial) 
          &
          (keyof MeshPhongMaterial)
        )[] = [
          'alphaMap',
          'alphaTest',
          'aoMap',
          'aoMapIntensity',
          'blendDst',
          'blendDstAlpha',
          'blendEquation',
          'blendEquationAlpha',
          'blendSrc',
          'blendSrcAlpha',
          'blending',
          'bumpMap',
          'bumpScale',
          'clipIntersection',
          'clipShadows',
          'clippingPlanes',
          'color',
          'colorWrite',
          'defines',
          'depthFunc',
          'depthTest',
          'depthWrite',
          'displacementBias',
          'displacementMap',
          'displacementScale',
          'dithering',
          'emissive',
          'emissiveIntensity',
          'emissiveMap',
          'envMap',
          'flatShading',
          'fog',
          'lightMap',
          'lightMapIntensity',
          'map',
          'morphNormals',
          'morphTargets',
          'name',
          // 'normalMap',
          'normalMapType',
          'normalScale',
          'opacity',
          'polygonOffset',
          'polygonOffsetFactor',
          'polygonOffsetUnits',
          'precision',
          'premultipliedAlpha',
          'refractionRatio',
          'shadowSide',
          'side',
          'skinning',
          'stencilFail',
          'stencilFunc',
          'stencilRef',
          'stencilWrite',
          'stencilZFail',
          'stencilZPass',
          'toneMapped',
          'transparent',
          'userData',
          'uuid',
          'version',
          'vertexColors',
          'visible',
          'wireframe',
          'wireframeLinecap',
          'wireframeLinejoin',
          'wireframeLinewidth',
        ]
        propertiesToCopy.forEach(prop=>{
          if(isWritable(newMaterial, prop)){
            //@ts-ignore-next-line
            newMaterial[prop] = cloneDeep(material[prop]);
          }
        })
        newMaterial.shininess = 2.2;
        // newMaterial.reflectivity = 0;
        // newMaterial.combine = 0;
        // newMaterial.shininess = 0;
        // newMaterial.normalScale = new Vector2(-0.3, -0.3);
        newMaterial.polygonOffset = true;
        newMaterial.polygonOffsetFactor = -1;
        newMaterial.polygonOffsetUnits = -8;
        
        el.material = newMaterial;
      }

      if(el.material?.name === "M_Eye_Gloss"){
        if(this.eyeGlossMaterial){
          el.material = this.eyeGlossMaterial;
        } else { 
          const material = new MeshPhongMaterial({color: new Color('white')});
          material.envMap = this.eyeEnvMap;
          material.transparent = true;
          material.blending = AdditiveBlending;
          material.polygonOffset = true;
          material.polygonOffsetFactor = 6.5;
          material.polygonOffsetUnit = 16.1;
          material.skinning = true;
          material.combine = MixOperation;
          this.eyeGlossMaterial = el.material = material
        }
      } 
      if(el.material?.name === "Lenses"){
        // el.material?.envMap = this.eyeEnvMap;
        // el.material?.shininess = 36.72;
        // el.material?.envMapIntensity = 3;
        // el.material.blending = AdditiveBlending;
      }
     
      if(el.material?.map) {
        this.renderer.initTexture(el.material.map)
      }
      if(el.material?.envMap){
        this.renderer.initTexture(el.material.envMap)
      }
      if(el.material?.roughnessMap){
        this.renderer.initTexture(el.material.roughnessMap)
      }
    })
    
    this.loaded = true;

  }
  public init(spotLight: SpotLight){
    this.eyeSpotLight = spotLight;
    this.meshGroup = this.gltf.scene;
    this.meshGroup.scale.set(68,68,68);
    this.meshGroup.position.set(-1, 0, -22);
    this.meshGroup.rotation.set(0, MathUtils.degToRad(180), 0);
    this.meshGroup.traverse(child=>{  
      if(child instanceof Mesh){
        child.material?.fog = false;
      }
    })
    this.makeAnimations();
    this.addTurnST();
    this.scene.add(this.meshGroup);
    this.isInit = true;
  }
  public onRender(clock: Clock){
  }
  public onResize(){
    this.turnST?.kill();
    clearTimeout(this.resizeTimer);
    this.resizeTimer = setTimeout(()=>{
      this.addTurnST();
    })
  }
  public setupGui(gui: GUI){
    const folder = gui.addFolder('Saharov');
    if(this.action){
      folder.add(this.action, 'paused')
    }
    folder.add(this.meshGroup.position, 'x', -100, 100, 1)
    folder.add(this.meshGroup.position, 'y', -100, 100, 1)
    folder.add(this.meshGroup.position, 'z', -100, 100, 1)
    
    folder.add(new DegRadHelper(this.meshGroup.rotation, 'y'),'value', 0, 360).name('Rotate Y');
    folder
      .add({'scale': this.meshGroup.scale.x}, 'scale', 0.1, 100, 0.01)
      .onChange(scale=>this.meshGroup.scale.set(scale, scale, scale));
    if(this.head){
      folder.add({'Head normal scale': this.head.material.normalScale.x}, 'Head normal scale', -3, 3, 0.1).onChange(scale=>{
        this.head.material.normalScale = new Vector2(scale, scale);
      })
    }
    if(this.coat){
      const coatFolder = folder.addFolder('Coat')
      coatFolder.add({'Normal scale': this.coat.material.normalScale.x}, 'Normal scale', -3, 3, 0.1).onChange(scale=>{
        this.coat.material.normalScale = new Vector2(scale, scale);
      })
      coatFolder.add(this.coat.material, 'shininess', 0, 50, 0.1)
      coatFolder.add(this.coat.material, 'polygonOffset')
      coatFolder.add(this.coat.material, 'polygonOffsetFactor', -30, 30, 0.1)
      coatFolder.add(this.coat.material, 'polygonOffsetUnits', -30, 30, 0.1)
    }

    if(this.hatMaterial){
      const hatFolder = folder.addFolder('Hat');
      hatFolder.add(this.hatMaterial, 'polygonOffset')
      hatFolder.add(this.hatMaterial, 'polygonOffsetFactor', -30, 30, 0.1)
      hatFolder.add(this.hatMaterial, 'polygonOffsetUnits', -30, 30, 0.1)
    }
    if(this.legsMaterial){
      const legsFolder = folder.addFolder('Legs');
      legsFolder.add(this.legsMaterial, 'polygonOffset')
      legsFolder.add(this.legsMaterial, 'polygonOffsetFactor', -30, 30, 0.1)
      legsFolder.add(this.legsMaterial, 'polygonOffsetUnits', -30, 30, 0.1)
    }

    if(this.eyeGlossMaterial){
      const eyeFolder = folder.addFolder('Eye')
      eyeFolder.add(this.eyeGlossMaterial, 'polygonOffset')
      eyeFolder.add(this.eyeGlossMaterial, 'polygonOffsetFactor', -30, 30, 0.1)
      eyeFolder.add(this.eyeGlossMaterial, 'polygonOffsetUnits', -30, 30, 0.1)
      // eyeFolder.add(this.eyeGlossMaterial, 'envMapIntensity', 0, 30, 0.1)
      // eyeFolder.add(this.eyeGlossMaterial, 'reflectivity', 0, 30, 0.01)
      // eyeFolder.add(this.eyeGlossMaterial, 'shininess', 0, 100, 0.1)
      // eyeFolder.add(this.eyeGlossMaterial, 'roughness', 0, 1, 0.1)
      eyeFolder.add(this.eyeGlossMaterial, 'opacity', 0, 1, 0.1)
      if(this.eyeGlossMaterial.envMapIntensity){
        eyeFolder.add(this.eyeGlossMaterial, 'envMapIntensity', 0, 10, 0.01);
      }
      if(this.eyeGlossMaterial.reflectivity){
        eyeFolder.add(this.eyeGlossMaterial, 'reflectivity', 0, 1, 0.01);
      }
      eyeFolder.add(this.eyeGlossMaterial, 'shininess', 0, 100, 0.1);
    }
    
    
    // folder.add(this.glasses.material, 'roughness', 0, 1, 0.1)
  }

  private makeAnimations(){
    this.clip = this.gltf.animations[0];
    if(!this.clip) return;
    this.mixer = new AnimationMixer(this.meshGroup);
    this.action = this.mixer.clipAction(this.clip);
    this.action.loop = LoopPingPong;
    this.action.play();
    const startOffset = 0;
    const appearanceDuration = 0.032;
    const turnStartTime = 0.4596774194;
    const turnEndTime = 0.6428571429;
    const lastCycleStartTime = 0.6428571429;
    const lastCycleEndTime = 0.8052995392;
    const totalTime = {t:0};
    this.mixer.setTime(totalTime.t);
    const totalDuration = this.clip.duration * this.action.timeScale;
    const updateMixerTime = ()=>{
      if(totalTime.t < totalDuration){
        this.mixer.setTime(totalTime.t);
      }
    }
    const commonTl = gsap.timeline({paused:true}).to(totalTime, {t:totalDuration, duration:totalDuration, ease:'none', onUpdate: updateMixerTime})
    
    this.appearTl = commonTl.tweenFromTo(startOffset, appearanceDuration * totalDuration, {
      immediateRender: false, 
      paused: false, 
      ease: 'none',
      delay: 1.5,
      onComplete:()=>this.loopedTl.play(),
    })
    this.loopedTl = commonTl.tweenFromTo(appearanceDuration * totalDuration, turnStartTime * totalDuration, {
      immediateRender: false, 
      paused: true, 
      ease: 'none',
      onComplete:()=>{
        if(this.turnRequired){
          this.playTurn();
        } else {
          this.loopedTl.timeScale(1);
          this.loopedTl.reverse();
        }
      },
      onReverseComplete: ()=>{
        this.loopedTl.play();
      },
    })

    this.turnTl = commonTl.tweenFromTo(turnStartTime * totalDuration, turnEndTime * totalDuration, {
      immediateRender: false,
      paused: true,
      ease: 'none',
      onComplete:()=>{
        if(this.turnRequired){
          this.finalLoopedTl.play();
        } else {
          this.reverseTurn();
        }
      },
      onReverseComplete:()=>{
        if(this.turnRequired){
          this.playTurn();
        } else {
          this.loopedTl.reverse();
        }
      },
      onUpdate:()=>{
        updateMixerTime();
        const startProgress = 0.823;
        const endProgress = 1;
        const totalProgress = this.turnTl.totalProgress() || 0;
        const localProgress = Math.max(0, (totalProgress - startProgress) / (endProgress - startProgress));
        const maxValue = 7;
        this.eyeSpotLight.intensity = localProgress * maxValue;
      }
    })
    

    this.finalLoopedTl = commonTl.tweenFromTo(lastCycleStartTime * totalDuration, totalDuration * lastCycleEndTime, {
      immediateRender: false,
      paused: true, 
      ease: 'none',
      onReverseComplete:()=>{
        if(this.turnRequired){
          this.finalLoopedTl.play();
        } else {
          this.reverseTurn()
        }
      },
      onComplete:()=>{
        this.finalLoopedTl.reverse();
      }
    })

    this.tweens = [
      this.appearTl,
      this.loopedTl,
      this.turnTl,
      this.finalLoopedTl
    ];
  }
  private addTurnST(){
    this.turnST = ScrollTrigger.create({
      trigger: '#scrollableContent',
      start: '90% bottom',
      onLeaveBack: ()=>{
        this.requireTurnAnimation(false);
      },
      onEnter:()=>{
        this.requireTurnAnimation(true);
      },
    })
  }
  private requireTurnAnimation(showTurn: boolean){
    this.turnRequired = showTurn;
    const activeTween = this.activeTween;
    activeTween?.reversed(!showTurn);
  }
  private playTurn(){
    this.turnTl.timeScale(1.3).play();
  }
  private reverseTurn(){
    this.turnTl.timeScale(1).reverse();
  }
}
