import ScrollTrigger from 'gsap/ScrollTrigger';
import gsap from 'gsap';
import { GUI } from "dat.gui";
import { CircleGeometry, Color, DirectionalLight, Group, MathUtils, Mesh, MeshBasicMaterial, PointLight, Scene, SphereGeometry, SpotLight, SpotLightHelper, Vector3 } from "three";
import { DegRadHelper } from "./helpers/gui";

const autoBind = require('auto-bind');

export default class LightGroup{
  spotLight: SpotLight;
  spotLightForEye: SpotLight;
  spotLightHelper: SpotLightHelper;
  spotLightForEyeHelper: SpotLightHelper;
  pointLight1: PointLight;
  pointLight2: PointLight;
  pointLight3: PointLight;
  _visible = true;
  lightST: gsap.plugins.ScrollTriggerInstance;
  lightTL: gsap.core.Timeline | gsap.core.Tween;
  resizeTimer: number;

  set visible(value: boolean){
    this.spotLight.visible = value;
    this.spotLightForEye.visible = value;
    this.pointLight1.visible = value;
    this.pointLight2.visible = value;
    this.pointLight3.visible = value;
    this.spotLightHelper.visible = value;
    this.spotLightForEyeHelper.visible = value;
  }

  get visible(){
    return this._visible;
  }

  constructor(private scene: Scene){
    autoBind(this);
    this.createLightsObjects();
    this.createTL();
    this.createST();
  }

  createLightsObjects(){
    //SPOT LIGHT
    this.spotLight = new SpotLight( 0xffffff, 1);
    this.spotLight.target.position.set(0,0,0);
    this.spotLight.position.set(-120, 150, -170);
    // this.spotLight.position.set(-551, 269, -371);
    // this.spotLight.position.set(-724, 172, -371);
    this.spotLight.intensity = 0;
    this.spotLight.angle = MathUtils.degToRad(22);
    this.spotLight.penumbra = 0.46;
    this.scene.add(this.spotLight)
    this.scene.add(this.spotLight.target);
    this.spotLight.target.position.set(0,63,0);
    this.spotLight.target.updateMatrixWorld();

    //LIGHT HELPER
    this.spotLightHelper = new SpotLightHelper(this.spotLight, new Color('red'));
    this.scene.add(this.spotLightHelper)
    this.spotLightHelper.update();


    this.spotLightForEye = new SpotLight(0xffffff, 1);
    this.spotLightForEye.position.set(-13, 110, -59);
    this.spotLightForEye.intensity = 0;
    this.spotLightForEye.angle = MathUtils.degToRad(0.6);
    this.spotLightForEye.penumbra = 0.46;
    this.spotLightForEye.visible = false;
    this.scene.add(this.spotLightForEye)
    this.scene.add(this.spotLightForEye.target);
    this.spotLightForEye.target.position.set(5,116,-21);
    this.spotLightForEye.target.updateMatrixWorld();

    this.spotLightForEyeHelper = new SpotLightHelper(this.spotLightForEye, new Color('red'));
    this.scene.add(this.spotLightForEyeHelper)
    this.spotLightForEyeHelper.update();


    //POINT LIGHT
    this.pointLight1 = new PointLight(0xffffff, 0.8, 400)
    this.pointLight1.position.set(-13, 712, 240)
    this.pointLight1.intensity = 10;
    this.pointLight1.distance = 1000;
    this.pointLight1.add( new Mesh( new SphereGeometry( 10, 32, 32 ), new MeshBasicMaterial( { color: 0xff0000 } ) ) );
    this.scene.add(this.pointLight1);

    //POINT LIGHT 2
    this.pointLight2 = new PointLight(0xffffff, 0.8, 400)
    this.pointLight2.position.set(-13, 310, 883)
    this.pointLight2.intensity = 10;
    this.pointLight2.distance = 1000;
    this.pointLight2.add( new Mesh( new SphereGeometry( 10, 32, 32 ), new MeshBasicMaterial( { color: 0x0000ff } ) ) );
    this.scene.add(this.pointLight2);

    //POINT LIGHT 3
    this.pointLight3 = new PointLight(0xffffff, 0.8, 400)
    this.pointLight3.position.set(-13, 303, 235)
    this.pointLight3.intensity = 9.2;
    this.pointLight3.distance = 471;
    this.pointLight3.add( new Mesh( new SphereGeometry( 10, 32, 32 ), new MeshBasicMaterial( { color: 0x00ff00 } ) ) );
    this.scene.add(this.pointLight3);
  }

  createTL(){
    this.lightTL = gsap.timeline({paused: true})
      // .fromTo(this.spotLight.position, {x:-120, y:150, z:-170}, {x: -551, y:269, z:-371, duration: 1, ease: 'none'}, 0)
      .fromTo(this.spotLight, {intensity: 0.8}, {intensity: 0.3, duration: 1, ease: 'none'}, 0)

      .fromTo(this.pointLight1.position, {x: -13, y:712, z:240}, {x: -192, y: 258, z: -192, duration: 1, ease: 'none'}, 0)
      .fromTo(this.pointLight1, {intensity: 10, distance: 1000}, {intensity: 2.9, distance: 449, ease: 'none'}, 0)

      .fromTo(this.pointLight2.position, {x:-13, y:310, z: 883}, {x: -102, y: 427, z: -349, duration: 1, ease: 'none'}, 0)
      .fromTo(this.pointLight2, {intensity: 10, distance: 1000}, {intensity: 0.6, distance: 539, ease: 'none'}, 0)

      .fromTo(this.pointLight3.position, {x:-13, y:303, z:235}, {x: -13, y: 310, z: -217, duration: 1, ease: 'none'}, 0)
      .fromTo(this.pointLight3, {intensity: 9.2, distance: 471}, {intensity: 0.4, distance: 540, ease: 'none'}, 0)
  }
  createST(){
    this.lightST = ScrollTrigger.create({
      trigger: '#scrollableContent',
      start: '20% top',
      end: "bottom bottom",
      onUpdate:(self)=>{
        this.lightTL?.totalProgress(self.progress)
      },
      onLeaveBack:()=>{
        this.lightTL?.totalProgress(0);
      },
      onLeave:()=>{
        this.lightTL?.totalProgress(1);
      }
    })
  }

  setupGui(gui: GUI){
    const folder = gui.addFolder("Light");
    const controller = folder.add({'Visible': true}, 'Visible').onChange(visible=>{
      this.spotLight.visible = visible;
      this.pointLight1.visible = visible;
      this.pointLight2.visible = visible;
      this.pointLight3.visible = visible;
      this.spotLightHelper.visible = visible;
    });
    // controller.setValue(false);
    
    const spotLightFolder = this.makeLightGUI(folder, this.spotLight, 'Spot Light', undefined, ()=>this.spotLightHelper?.update());
    const spotLightHelperController = spotLightFolder.add({"Helper": true}, 'Helper').onChange(enabled=>{
      this.spotLightHelper.visible = enabled;
    })
    spotLightHelperController.setValue(false);

    const spotLightEyeFolder = this.makeLightGUI(folder, this.spotLightForEye, 'Eye light', undefined, ()=>this.spotLightForEyeHelper?.update());
    const spotLightEyeHelperController = spotLightEyeFolder.add({"Helper": true}, 'Helper').onChange(enabled=>{
      this.spotLightForEyeHelper.visible = enabled;
    })
    spotLightEyeHelperController.setValue(false);
    this.makeLightGUI(folder, this.pointLight1, 'Point Light 1');
    this.makeLightGUI(folder, this.pointLight2, 'Point Light 2');
    this.makeLightGUI(folder, this.pointLight3, 'Point Light 3');
    
  }
  makeLightGUI(gui: dat.GUI, light: THREE.Light, name:string, radius=1000, onChange: (p:number)=>void = (p)=>{}) {
    const folder = gui.addFolder(name);
    folder.add(light, 'visible').listen();
    folder.add(light.position, 'x', -radius, radius).onChange(onChange).listen();
    folder.add(light.position, 'y', 0, radius).onChange(onChange).listen();
    folder.add(light.position, 'z', -radius, radius).onChange(onChange).listen();
    folder.add(light, 'intensity', 0, 10, 0.1).onChange(onChange).listen();
    if(light instanceof PointLight){
      folder.add(light, 'distance', 0, 1000, 1).onChange(onChange).listen();
    }
    if(light instanceof DirectionalLight || light instanceof SpotLight){
      const onTargeChange = (p: number)=>{
        light.target.updateMatrixWorld();
        onChange(p)
      }
      folder.add(light.target.position, 'x', -100, 200, 1).name('Target X').onChange(onTargeChange).listen();
      folder.add(light.target.position, 'y', -100, 200, 1).name('Target Y').onChange(onTargeChange).listen();
      folder.add(light.target.position, 'z', -100, 200, 1).name('Target Z').onChange(onTargeChange).listen();
    }
    if(light instanceof SpotLight){
      folder.add(new DegRadHelper(light, 'angle'), 'value', 0, 10, 0.01).name('angle').onChange(onChange).listen();
      folder.add(light, 'penumbra', 0, 1, 0.01).onChange(onChange).listen();
    }
    if(!(light instanceof SpotLight)){
      const helperVisible = folder.add({'Helper': true}, 'Helper').onChange(enabled=>{
        light.children.forEach(child=>{
         child.visible = enabled;
        })
      })
      helperVisible.setValue(false)
    }

    return folder;
  }

  public onResize(){
    this.lightST.kill()
    clearTimeout(this.resizeTimer)
    this.resizeTimer = setTimeout(this.createST, 100)
  }
}