import {GUI} from 'dat.gui';
import autoBind from 'auto-bind';
import { Clock, Color, HalfFloatType, LinearFilter, LoadingManager, Mesh, MeshBasicMaterial, MeshNormalMaterial, OrthographicCamera, PerspectiveCamera, RawShaderMaterial, RGBAFormat, RGBFormat, Scene, ShaderMaterial, sRGBEncoding, Texture, Vector2, WebGLRenderer, WebGLRenderTarget, RGBADepthPacking } from "three";
import { EffectPass, RenderPass, ShaderPass, EffectComposer,ChromaticAberrationEffect, GammaCorrectionEffect, Effect, ClearPass, BloomEffect, KernelSize, BlendFunction, NoiseEffect, TextureEffect, SavePass, SMAAEffect, SMAAImageLoader,ScanlineEffect, SMAAPreset, EdgeDetectionMode, BlurPass, PixelationEffect, Pass, LambdaPass, ColorAverageEffect, BrightnessContrastEffect, DepthPass, DepthDownsamplingPass, CustomDepthPass, NormalPass, SSAOEffect, OverrideMaterialManager } from 'postprocessing';
import textsMergeVertShader from '../assets/shaders/textsMerge.vert';
import textsMergeFragShader from '../assets/shaders/textsMerge.frag';
import noise3Vert from '../assets/shaders/noise3.vert';
import noise3Frag from '../assets/shaders/noise3.frag';
import unsharMaskVertShader from '../assets/shaders/unsharpMask.vert';
import unsharMaskFragShader from '../assets/shaders/unsharpMask.frag';


// OverrideMaterialManager.workaroundEnabled = true;

export default class PostprocessingGroup{
  composer: EffectComposer;
  textPlane: Mesh;
  //passes
  sceneRenderPass: RenderPass;
  saveDefaultRenderPass: SavePass;
  mergeTextAndScenePass: ShaderPass;
  filmPass: EffectPass;
  noisePass: ShaderPass;
  blurPass: BlurPass;
  blurSavePass: SavePass;
  blurTexturePass: EffectPass;
  smaaPass: EffectPass;
  chromaticAberrationPass: EffectPass;
  unsharpPass: EffectPass;

  //effects
  bloomEffect: BloomEffect;
  mergeTextAndSceneEffect: ShaderMaterial;
  customNoiseEffect: ShaderMaterial;
  noiseEffect: NoiseEffect;
  scanlineEffect: ScanlineEffect;
  blurTextureEffect: TextureEffect;
  pixelationEffect: PixelationEffect;
  chromaticAberrationEffect: ChromaticAberrationEffect;
  colorAverageAffect: ColorAverageEffect;
  brightnessContrastEffect: BrightnessContrastEffect;
  unsharpEffect: ShaderMaterial;

  SMAA_assets: Map<string,any>;
  clock = new Clock();
  camera: PerspectiveCamera;
  scene: Scene;
  renderer: WebGLRenderer;
  passes: Pass[] = [];
  loaded = false;
 
  
  constructor(props: {scene:Scene, camera:PerspectiveCamera ,renderer: WebGLRenderer}){
    autoBind(this)
    const {scene, camera, renderer} = props;
    this.camera = camera;
    this.scene = scene;
    this.renderer = renderer;
    this.composer = new EffectComposer(renderer, {
      frameBufferType: HalfFloatType
    })
  }

  public async load(){
    this.SMAA_assets = await this.loadSMAA();
    this.loaded = true;
  }

  public async init(textTexture: Texture, particlesTexture: Texture){ 
    textTexture.minFilter = LinearFilter;
    textTexture.magFilter = LinearFilter;
        
    const sceneRenderPass = new RenderPass(this.scene, this.camera)    
    this.composer.addPass(sceneRenderPass)
    this.passes.push(sceneRenderPass);

    const sceneRenderTarget = new WebGLRenderTarget(0, 0, {format: RGBFormat, minFilter: LinearFilter, magFilter: LinearFilter});
    this.saveDefaultRenderPass = new SavePass(sceneRenderTarget, true);
    this.composer.addPass(this.saveDefaultRenderPass)
    this.passes.push(this.saveDefaultRenderPass);
    
    
    this.addUnsharpPass();
    this.addFilmPass();
    this.addBlurPass();
    this.addNoisePass();
    
    this.addMergeTextPass(textTexture, sceneRenderTarget.texture, particlesTexture);
    this.addSMAAPass();   
  }

  private addUnsharpPass(){
    this.unsharpEffect = new ShaderMaterial({
      uniforms: {
        inputBuffer: {value: null},
        size: {value: 1},
        resolution: {value: [ this.renderer.domElement.width, this.renderer.domElement.height ]},
      },
      vertexShader: unsharMaskVertShader,
      fragmentShader: unsharMaskFragShader,
    })
    this.unsharpPass = new ShaderPass(this.unsharpEffect)
    this.composer.addPass(this.unsharpPass);
  }

  private addFilmPass(){
    this.colorAverageAffect = new ColorAverageEffect(BlendFunction.NORMAL);
    this.brightnessContrastEffect = new BrightnessContrastEffect()

    //BLOOM
    this.bloomEffect = new BloomEffect({
      blendFunction: BlendFunction.SCREEN,
      kernelSize: KernelSize.MEDIUM,
      luminanceThreshold: 0.8,
      luminanceSmoothing: 0.075,
      height: 480,
      intensity: 0.65,      
    });
    //SCANLINE
    this.scanlineEffect = new ScanlineEffect({
      blendFunction: BlendFunction.OVERLAY,
			density: 1.723
    })
    this.scanlineEffect.blendMode.opacity.value = 0.07;
    //PIXELATION
    this.pixelationEffect = new PixelationEffect(0.9);

    //FILM PASS
    this.filmPass = new EffectPass(this.camera, this.colorAverageAffect, this.bloomEffect, this.pixelationEffect, this.brightnessContrastEffect );
    this.composer.addPass(this.filmPass);

    this.passes = [...this.passes, this.filmPass];
  }

  private addBlurPass(){
    //save state before blur
    const savePass = new SavePass();
    this.composer.addPass(savePass);
    this.passes.push(savePass);
    
    //apply blur
    const blurPass = new BlurPass({
      height: 480,
      karnelSize: KernelSize.LARGE,
    });
    blurPass.scale = 0.04
    this.composer.addPass(blurPass);
    this.passes.push(blurPass);

    const drawTextureEffect = new TextureEffect({
			texture: savePass.renderTarget.texture
    });
    drawTextureEffect.blendMode.opacity.value = 0.61;
    const drawTexturePass = new EffectPass(this.camera, drawTextureEffect);
    this.composer.addPass(drawTexturePass);
    this.passes.push(blurPass);

    this.blurPass = blurPass;
    this.blurSavePass = savePass;
    this.blurTexturePass = drawTexturePass;
    this.blurTextureEffect = drawTextureEffect;
  }

  private addNoisePass(){
     //NOISE (CUSTOM)
     this.customNoiseEffect = new ShaderMaterial({
      uniforms: {
        inputBuffer: {value: null},
        resolution: {value: [ this.renderer.domElement.width, this.renderer.domElement.height ]},
        time: {value: this.clock.getElapsedTime()},
        timeScale: {value: 10},
        grainSize: {value: 5},
        grainBrightness: {value: 1},
        opacity: {value: 1},
        BGgrainSize: {value: 25},
        BGgrainBrightness: {value: 3},
        BGopacity: {value: 1},
        BGLightness: {value: 0.87},
        useBlendMode: {value: false},
        backgroundColor: {value: this.scene.background},
      },
      vertexShader: noise3Vert,
      fragmentShader: noise3Frag,
    })
    const customNoisePass = new ShaderPass(this.customNoiseEffect)
    this.composer.addPass(customNoisePass)
  }

  private addSnowPass(particlesTexture: Texture){
    const textureEffect = new TextureEffect({texture: particlesTexture, blendFunction: BlendFunction.NORMAL});
    const particlesPass = new EffectPass(this.camera, textureEffect)
    this.composer.addPass(particlesPass);
  }

  private addMergeTextPass(textTexture: Texture, sceneTexture: Texture, particlesTexture: Texture){
    textTexture.minFilter = LinearFilter;
    textTexture.magFilter = LinearFilter;
    sceneTexture.minFilter = LinearFilter;
    sceneTexture.magFilter = LinearFilter;
    // Вообще можно просто добавить эффект к предыдущему pass, просто Effect с кастомным шейдером
    this.mergeTextAndSceneEffect = new ShaderMaterial({
      uniforms: {
        inputBuffer: {value: null},
        sceneTexture: {value: sceneTexture},
        textsTexture: {value: textTexture},
        particlesTexture: {value: particlesTexture},
        backgroundColor: {value: this.scene.background},
        resolution: {value: [ this.renderer.domElement.width, this.renderer.domElement.height ]},
        whiteColor: {value: new Color('#D8D8D8')},
        blackColor: {value: new Color('#000000')},
        snowOpacity: {value: 0.8},
      },
      vertexShader: textsMergeVertShader,
      fragmentShader: textsMergeFragShader,
    })
    this.mergeTextAndScenePass = new ShaderPass(this.mergeTextAndSceneEffect)  
    this.composer.addPass(this.mergeTextAndScenePass);
    this.passes.push(this.mergeTextAndScenePass);
  }

  private addSMAAPass(){
    const smaaEffect = new SMAAEffect(
      this.SMAA_assets.get("smaa-search"),
      this.SMAA_assets.get("smaa-area"),
      SMAAPreset.ULTRA,
			EdgeDetectionMode.COLOR
    );
    // smaaEffect.edgeDetectionMaterial.setEdgeDetectionThreshold(0.01);
    // smaaEffect.edgeDetectionMaterial.setLocalContrastAdaptationFactor(100);
    const smaaPass = new EffectPass(this.camera, smaaEffect);
    this.composer.addPass(smaaPass);
    this.smaaPass = smaaPass;    
    this.passes.push(smaaPass);
  }

  private loadSMAA() {
    const assets = new Map();
    const loadingManager = new LoadingManager();
    const smaaImageLoader = new SMAAImageLoader(loadingManager);
  
    return new Promise<Map<string, any>>((resolve, reject) => {
  
      loadingManager.onLoad = () => resolve(assets);
      loadingManager.onError = reject;
  
      smaaImageLoader.load(([search, area]) => {
  
        assets.set("smaa-search", search);
        assets.set("smaa-area", area);
 
      });
  
    });
  
  }

  public onResize(){
    const w = window.innerWidth;
    const h = window.innerHeight;
    this.composer.setSize(w, h);
    if(this.mergeTextAndSceneEffect){
      this.mergeTextAndSceneEffect.uniforms.resolution.value = [ w, h ];
    }
    if(this.customNoiseEffect){
      this.customNoiseEffect.uniforms.resolution.value = [ w, h ];
    }
    if(this.unsharpEffect){
      this.unsharpEffect.uniforms.resolution.value = [ w, h ];
    }
  }

  public onRender(clock: Clock){
    this.composer.render(clock.getDelta());
    if(this.customNoiseEffect){
      this.customNoiseEffect.uniforms.time.value = this.clock.getElapsedTime();
    }
  }

  public updateSize(){
    this.composer.setSize();
    this.onResize();
  }

  setSize(w:number,h:number){

  }

  public setupGui(gui: dat.GUI){
    const postprocessing = gui.addFolder('Postprocessing');

    postprocessing.add({'Enabled': true}, "Enabled").onChange((enabled)=>{
        this.passes.forEach(pass=>{
          if(!(pass instanceof RenderPass) && !(pass instanceof SavePass) && pass !== this.mergeTextAndScenePass && pass !== this.smaaPass){
            pass.enabled = enabled;
          }
        })
    })
    if(this.mergeTextAndScenePass){
      postprocessing.add(this.mergeTextAndScenePass, 'enabled').listen().name('Display text').onChange(enabled=>{
        this.saveDefaultRenderPass.enabled = enabled;
      })
      postprocessing.add({'Snow Opacity': this.mergeTextAndSceneEffect.uniforms.snowOpacity.value},'Snow Opacity', 0, 1).onChange((value)=>{
        this.mergeTextAndSceneEffect.uniforms.snowOpacity.value = value;
      })
    }
    if(this.filmPass){
      postprocessing.add(this.filmPass, 'enabled').listen().name('Film pass').onChange(enabled=>{
        this.noisePass.enabled = enabled;
      });
    }
    if(this.blurPass){
      postprocessing.add(this.blurPass, 'enabled').listen().name('Blur pass').onChange(enabled=>{
        this.blurSavePass.enabled = enabled;
        this.blurTexturePass.enabled = enabled;
      });
    }
    if(this.smaaPass){  
      postprocessing.add(this.smaaPass, 'enabled').listen().name('Antialise pass').onChange(enabled=>{
        this.mergeTextAndScenePass.renderToScreen = !enabled;
        this.smaaPass.renderToScreen = enabled;
      });
    }
    if(this.chromaticAberrationPass){
      postprocessing.add(this.chromaticAberrationPass, 'enabled').listen().name('Chromatic Aberration')
    }

    if(this.unsharpPass){
      postprocessing.add(this.unsharpPass, 'enabled').listen().name('Unsharp pass')
    }



    if(this.unsharpEffect){
      postprocessing.add({'Unsharp radius': this.unsharpEffect.uniforms.size.value}, 'Unsharp radius', 0,10, 0.01).onChange(value=>{
        this.unsharpEffect.uniforms.size.value = value;
      });
    }

    if(this.colorAverageAffect){
      postprocessing.add({'Color Average': this.colorAverageAffect.blendMode.blendFunction}, "Color Average", BlendFunction).onChange((value) => {
  			this.colorAverageAffect.blendMode.setBlendFunction(Number(value));
      });      
    }
    if(this.scanlineEffect){
      postprocessing.add({'Scanline Density': this.scanlineEffect.getDensity()}, "Scanline Density", 0.001, 10, 0.001).onChange((value) => {
  			this.scanlineEffect.setDensity(value);
  		});

  		postprocessing.add(this.scanlineEffect.blendMode.opacity, 'value',0, 1, 0.01).name('Scanline Opacity')

  		postprocessing.add({'Scanline blend': this.scanlineEffect.blendMode.blendFunction}, "Scanline blend", BlendFunction).onChange((value) => {
  			this.scanlineEffect.blendMode.setBlendFunction(Number(value));
      });
    }
    if(this.blurPass){
      postprocessing.add(this.blurPass, "resolution", [240, 360, 480, 720, 1080]).name('Blur resolution');

  		postprocessing.add({'Blur KarnelSize': this.blurPass.kernelSize}, 'Blur KarnelSize', KernelSize).onChange((value) => {
  			this.blurPass.kernelSize = Number(value);
  		});

      postprocessing.add(this.blurPass, "scale", 0, 10, 0.01).name("Blur Scale")
      postprocessing.add({'Blur opacity': 1 - this.blurTextureEffect.blendMode.opacity.value}, 'Blur opacity', 0, 1, 0.01).onChange(value=>{
        this.blurTextureEffect.blendMode.opacity.value = 1 - value;
      })
    }
    if(this.pixelationEffect){
      postprocessing.add({'Pixelation': this.pixelationEffect.getGranularity()}, 'Pixelation', 0, 5, 0.01).onChange(value=>{
        this.pixelationEffect.setGranularity(value);
      })
    }
    if(this.customNoiseEffect){
      const noiseFolder = postprocessing.addFolder('noise');
      noiseFolder.add({'Time Scale': this.customNoiseEffect.uniforms.timeScale.value}, 'Time Scale', 1, 100, 0.1).onChange((timeScale)=>{
        this.customNoiseEffect.uniforms.timeScale.value = timeScale;
      })
      noiseFolder.add({'Grain Size': this.customNoiseEffect.uniforms.grainSize.value}, 'Grain Size', 0, 100, 0.1).onChange((size)=>{
        this.customNoiseEffect.uniforms.grainSize.value = size;
      })
      noiseFolder.add({'Opacity': this.customNoiseEffect.uniforms.opacity.value}, 'Opacity', 0, 1, 0.1).onChange((value)=>{
        this.customNoiseEffect.uniforms.opacity.value = value;
      })
      noiseFolder.add({'Grain Brightness': this.customNoiseEffect.uniforms.grainBrightness.value}, 'Grain Brightness', 0, 10, 0.1).onChange((value)=>{
        this.customNoiseEffect.uniforms.grainBrightness.value = value;
      })
      noiseFolder.add({'BG Grain Size': this.customNoiseEffect.uniforms.BGgrainSize.value}, 'BG Grain Size', 0, 100, 0.1).onChange((size)=>{
        this.customNoiseEffect.uniforms.BGgrainSize.value = size;
      })
      noiseFolder.add({'BG Opacity': this.customNoiseEffect.uniforms.BGopacity.value}, 'BG Opacity', 0, 1, 0.1).onChange((value)=>{
        this.customNoiseEffect.uniforms.BGopacity.value = value;
      })
      noiseFolder.add({'BG Grain Brightness': this.customNoiseEffect.uniforms.BGgrainBrightness.value}, 'BG Grain Brightness', 0, 10, 0.1).onChange((value)=>{
        this.customNoiseEffect.uniforms.BGgrainBrightness.value = value;
      })
      noiseFolder.add({'BG Lightness': this.customNoiseEffect.uniforms.BGLightness.value}, 'BG Lightness', 0.5, 1, 0.01).onChange((value)=>{
        this.customNoiseEffect.uniforms.BGLightness.value = value;
      })
      
      // noiseFolder.add({'Normal blending': this.customNoiseEffect.uniforms.useBlendMode.value}, 'Normal blending').onChange((value)=>{
      //   this.customNoiseEffect.uniforms.useBlendMode.value = value;
      // })
    }
    if(this.brightnessContrastEffect){
      const brightnessFolder = postprocessing.addFolder('Brightness & Contrast');

      brightnessFolder.add({'Brightness': this.brightnessContrastEffect.uniforms.get("brightness").value}, "Brightness", -1, 1, 0.001).onChange((value) => {
        this.brightnessContrastEffect.uniforms.get("brightness").value = value;
      });
  
      brightnessFolder.add({'Contrast': this.brightnessContrastEffect.uniforms.get("contrast").value}, "Contrast", -1, 1, 0.001).onChange((value) => {
        this.brightnessContrastEffect.uniforms.get("contrast").value = value;
      });
  
      brightnessFolder.add({'Opacity': this.brightnessContrastEffect.blendMode.opacity.value}, "Opacity", 0, 1, 0.001).onChange((value) => {
        this.brightnessContrastEffect.blendMode.opacity.value = value;
      });
  
      brightnessFolder.add({'Blend mode': this.colorAverageAffect.blendMode.blendFunction}, "Blend mode", BlendFunction).onChange((value) => {
        this.brightnessContrastEffect.blendMode.setBlendFunction(Number(value));
      });
    }
    if(this.bloomEffect){
        const bloomEffectParams = {
		    	"resolution": this.bloomEffect.resolution.height,
		    	"kernel size": this.bloomEffect.blurPass.kernelSize,
		    	"blur scale": this.bloomEffect.blurPass.scale,
		    	"intensity": this.bloomEffect.intensity,
		    	"luminance": {
		    		"filter": this.bloomEffect.luminancePass.enabled,
		    		"threshold": this.bloomEffect.luminanceMaterial.threshold,
		    		"smoothing": this.bloomEffect.luminanceMaterial.smoothing
		    	}
        };
      
      const bloomEffectFolder = postprocessing.addFolder('Bloom')
      bloomEffectFolder.add(bloomEffectParams, "resolution", [240, 360, 480, 720, 1080]).onChange((resolution) => {
		  	this.bloomEffect.resolution.height = resolution;
		  });

		  bloomEffectFolder.add(bloomEffectParams, "kernel size", KernelSize).onChange((value) => {
		  	this.bloomEffect.blurPass.kernelSize = value
		  });

		  bloomEffectFolder.add(bloomEffectParams, "blur scale", 0, 10, 0.01).onChange((value) => {
		  	this.bloomEffect.blurPass.scale = value
		  });

		  bloomEffectFolder.add(bloomEffectParams, "intensity", 0, 3, 0.01).onChange((value) => {
		  	this.bloomEffect.intensity = value;
		  });

		  let folder = bloomEffectFolder.addFolder("Luminance");
		  folder.add(bloomEffectParams.luminance, "filter").onChange(() => {
		  	this.bloomEffect.luminancePass.enabled = bloomEffectParams.luminance.filter;
		  });

		  folder.add(bloomEffectParams.luminance, "threshold").min(0.0).max(1.0).step(0.001).onChange(() => {
		  	this.bloomEffect.luminanceMaterial.threshold = Number(bloomEffectParams.luminance.threshold);
		  });

		  folder.add(bloomEffectParams.luminance, "smoothing").min(0.0).max(1.0).step(0.001).onChange(() => {
		  	this.bloomEffect.luminanceMaterial.smoothing = Number(bloomEffectParams.luminance.smoothing);
      });
    }
  }
}