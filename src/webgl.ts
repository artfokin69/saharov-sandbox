import {PerspectiveCamera, WebGLRenderer, Scene, Clock, Color, PCFSoftShadowMap, sRGBEncoding, Vector3, NoToneMapping, LinearToneMapping, ReinhardToneMapping, CineonToneMapping, ACESFilmicToneMapping, Object3D, Mesh, WebGLRenderTarget, Fog, RGBAFormat, RGBFormat, LinearFilter, GammaEncoding, MeshBasicMaterial} from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import Stats from 'three/examples/jsm/libs/stats.module.js';
import * as dat from 'dat.gui';
import autoBind from 'auto-bind'
import {Inertia} from './helpers/inertia';
import Person from './Person';
import Stairs from './Stairs';
import PostprocessingGroup from './PostprocessingGroup';
import LightGroup from './LightGroup';
import Text from './text';
import { addKTX2Loader, disposeDracoLoader } from './helpers/loaders';
import scroll from './scroll';
import {isMobile, isTouch} from './helpers/media';
import Blind from './blind';
import Particles from './snow/particles';


const toneMappingOptions = {
  None: NoToneMapping,
  Linear: LinearToneMapping,
  Reinhard: ReinhardToneMapping,
  Cineon: CineonToneMapping,
  ACESFilmic: ACESFilmicToneMapping,
};

interface Mouse{
  x: number;
  y: number;
}

interface RectSize{
  width: number;
  height: number;
}

export default class App{
  container: HTMLDivElement;
  camera: PerspectiveCamera;
  renderer: WebGLRenderer;
  scene = new Scene();
  person:Person;
  stairs: Stairs;
  postprocessing: PostprocessingGroup;
  lightGroup: LightGroup;
  mouse = {x:0,y:0};
  mouseInertia  = {
    x: new Inertia(-1, 1, 0.4, 0.4, 0),
    y: new Inertia(-1, 1, 0.4, 0.4, 0)
  }
  clock = new Clock();
  stats = new Stats();
  orbitControls: OrbitControls;
  time: number = 0;
  text: Text;
  blind = new Blind();
  fog: Fog;
  settings = {
    parallax: {
      keepCameraTargetX: true,
      keepCameraTargetY: true,
      cameraHelper: false,
      limit: 30,
      friction: 0.4,
      acc: 0.4,
    },
    fogDensity: 100,
    scrollText: true,
  };
  winSize: RectSize = {
    width: window.innerWidth,
    height: window.innerHeight
  };
  resizeTimer: number;
  accParams = {
    x: 0,
    y: 0,
  }
  blindProgress = 0;
  cameraYPositionEnd:number;
  fps = 0;
  particles: Particles;

  constructor(container: HTMLDivElement){
    autoBind(this);
    window.scrollTo(0,0);
    this.container = container;
    this.initThree();
    // this.fog = new Fog(this.scene.background as Color, 384, 872);
    this.fog = new Fog(this.scene.background as Color, 0, 0);
    this.text = new Text(this.renderer, this.scene, this.fog);
    this.stairs = new Stairs(this.renderer, this.camera, this.scene);
    this.person = new Person(this.renderer, this.scene);
    this.particles = new Particles(this.renderer,this.camera);
    this.postprocessing = new PostprocessingGroup({
      scene: this.scene,
      camera: this.camera,
      renderer: this.renderer
    });
    this.mouseInertia.x.acc = this.settings.parallax.acc;
    this.mouseInertia.y.acc = this.settings.parallax.acc;
    this.mouseInertia.x.friction = this.settings.parallax.friction;
    this.mouseInertia.y.friction = this.settings.parallax.friction;
  }
  public async load(){
    await Promise.all([this.stairs.load(), this.person.load(), this.postprocessing.load(), this.particles.load()]);
    disposeDracoLoader()
  }
  public async loadTexts(){
    await this.text.load();
  }
  public async init(){
    this.lightGroup = new LightGroup(this.scene);
    this.stairs.init();
    this.particles.init();
    this.person.init(this.lightGroup.spotLightForEye);
    this.postprocessing.init(this.text.texture, this.particles.texture);
    this.orbitControls = new OrbitControls( this.camera, document.querySelector('#scrollableContent') );
    this.orbitControls.target = new Vector3(0, 70, 0);
    this.orbitControls.enabled = false;
    this.camera.copy(this.stairs.camera);
    this.camera.position.set(0,147, -793)
    this.updateCamera();
    this.addResizeListener();
    document.querySelector('.stats')?.appendChild(this.stats.dom)
    this.setupGui();
    this.render();
    this.blind.on('progress', (progress)=>{
      this.stairs.onBlindUpdate(progress);
      this.text.onBlindUpdate(progress)
    })
    this.stairs.on('appearanceComplete', ()=>{
      scroll.enable();
    })
    if(isTouch){
      this.addAccelerometer();
    } else {
      this.addMouseListeners();
    }
    this.stairs.playPrerender();
  }

  public play(){
    let windowPr = window.devicePixelRatio;
    this.renderer.setPixelRatio(isMobile ? windowPr : Math.min(windowPr, 1.5));
    this.postprocessing.updateSize();
    setTimeout(()=>{
      this.text.showFirstScreen();
      this.stairs.playAppearance();
    }, 300);
  }
  private initThree(){
    this.scene.background = new Color( '#EDEDED' );
    this.camera = new PerspectiveCamera( 45, this.winSize.width / this.winSize.height, 0.3, 3000 )
    this.renderer = this.createRenderer();
  }
  private createRenderer(){
    const canvas = document.createElement( 'canvas' );
    let context;
    if ( window.WebGL2RenderingContext) {
      try{
      } catch {
        context = canvas.getContext( 'webgl2', { antialias: false } );  
      }
    } else {
      context = canvas.getContext( 'webgl', { antialias: false } );
    }

    const renderer = new WebGLRenderer( { 
      canvas, 
      context, 
      alpha: false,
      antialias: false,
      stencil: false,
      depth: false,
      // logarithmicDepthBuffer: true,
      powerPreference: 'high-performance'
    } );
    renderer.setPixelRatio(0.01);
    // renderer.setPixelRatio(windowPr);
    renderer.setSize( this.winSize.width, this.winSize.height);
    renderer.outputEncoding = GammaEncoding;
    renderer.autoClear = false;
    renderer.toneMapping = toneMappingOptions.Linear;
    renderer.shadowMap.type = PCFSoftShadowMap;
    // renderer.toneMappingExposure = 1;
    // renderer.shadowMap.enabled = true;
    // renderer.shadowMap.type = PCFSoftShadowMap;
    const canvasContainer = document.createElement('div');
    canvasContainer.appendChild(renderer.domElement )
    this.container.appendChild(canvasContainer);
    return renderer;
  }
  private addMouseListeners(){
    window.addEventListener( 'mousemove', (e)=>{
      this.mouse.x = ( e.clientX / window.innerWidth ) * 2 - 1;
      this.mouse.y = - ( e.clientY / window.innerHeight ) * 2 + 1;  
    })
  }
  private addResizeListener(){
    window.addEventListener('resize', this.resizeHandler);
  }
  private resizeHandler(){ 
    clearTimeout(this.resizeTimer);

    this.resizeTimer = setTimeout(()=>{
      const newWinSize:RectSize = {
        width: window.innerWidth,
        height: window.innerHeight,
      }
      const wrongResize = this.detectWrongResize(this.winSize, newWinSize);
      this.winSize = newWinSize;
      if(wrongResize) return;
      this.renderer.setSize( this.winSize.width, this.winSize.height);
      this.updateCamera();
      this.postprocessing.onResize();
      this.text.onResize();
      this.stairs.onResize()
      this.lightGroup.onResize();
      this.blind.onResize();
      this.person.onResize();
      this.particles.onResize();
    }, 100);
  }
  private detectWrongResize(oldWinSize:RectSize,newWinSize:RectSize){
    const onSideChanged = oldWinSize.width === newWinSize.width || oldWinSize.height === newWinSize.height;
    return isMobile && onSideChanged;
  }
  private updateCamera(){
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
  }
  private getCameraParallaxAngle(camera: PerspectiveCamera, delta: number, axis: 'x'|'y'){
    const centerInAxisPlane = new Vector3(0,0,0);
    centerInAxisPlane[axis] = camera.position[axis];
    const distance = camera.position.distanceTo(centerInAxisPlane);
    const angle = Math.atan(delta / distance);
    return angle;
  }
  private updateInertiaByMouse(){
    this.mouseInertia.x.update(this.mouse.x);
    this.mouseInertia.y.update(this.mouse.y);
  }
  private transformCameraByMouse(){
    this.approximateLimit();
    const {keepCameraTargetY, keepCameraTargetX, limit} = this.settings.parallax;

    const deltaX = this.mouseInertia.x.value * limit;
    const deltaY = this.mouseInertia.y.value * limit;
    this.camera.position.x = this.stairs.camera.position.x + deltaX;
    this.camera.position.y = this.stairs.camera.position.y - deltaY
    
    if(keepCameraTargetY){
      this.camera.rotation.x -= this.getCameraParallaxAngle(this.camera, deltaY, 'y');
    }
    if(keepCameraTargetX){
      this.camera.rotation.y += this.getCameraParallaxAngle(this.camera, deltaX, 'x');
    }
  }
  private approximateLimit(){
    const z = -1 * this.camera.position.z;
    const minLimit = 10;
    const maxLimit = 30;
    const minZ = 156;
    const maxZ = 790;
    this.settings.parallax.limit =  this.calcProgressValue(minLimit, maxLimit, minZ, maxZ, z);
    const maxFriction = 0.4;
    const minFriction = 0.2
    const computedFriction = this.calcProgressValue(minFriction, maxFriction, minZ, maxZ, z);
    const maxAcc = 0.4;
    const minAcc = 0.2
    const cumputedAcc = this.calcProgressValue(minAcc, maxAcc, minZ, maxZ, z);

    this.mouseInertia.x.acc = cumputedAcc;
    this.mouseInertia.y.acc = cumputedAcc;
    this.mouseInertia.x.friction = computedFriction;
    this.mouseInertia.y.friction = computedFriction;
  }
  private calcProgressValue(minValue:number, maxValue:number, minDistance:number, maxDistance: number, currentPos: number){
    return minValue + (maxValue - minValue) * (currentPos - minDistance) / (maxDistance - minDistance);
  }
  private copyCameraFromStairs(){
    this.camera.position.copy(this.stairs.camera.position);
    this.camera.quaternion.copy(this.stairs.camera.quaternion);
    this.camera.zoom = this.stairs.camera.zoom;
    // this.camera.copy(this.stairs.camera);
  }
  private updateCameraForMobile(){
    if(!isMobile) return;
    // this.camera.position.x = Math.max(-5, Math.min(this.camera.position.x, 5));
    this.camera.zoom = Math.min(this.camera.zoom, 1.1);
    
    const zShift = 50;
    const startProgress = 0.7;
    const endProgress = 1;
    
    if(this.stairs.scrollProgress > startProgress){
      const progress = Math.max(0, this.stairs.scrollProgress - startProgress) / (endProgress - startProgress);
      const delta = zShift * progress;
      this.camera.position.z += delta;
    }
  }

  //ACCELERATION
  private async addAccelerometer(){
    // https://kongmunist.medium.com/accessing-the-iphone-accelerometer-with-javascript-in-ios-14-and-13-e146d18bb175
    const createBox = ()=>{
      const box = document.createElement('button');
      box.style.position = 'fixed';
      box.style.zIndex = '100000';
      box.style.padding = '10px';
      box.style.bottom = '0'
      box.style.right = '0'
      box.style.background = '#fff';
      box.style.fontSize = '12px'
      box.style.color = 'black'
      document.body.appendChild(box);
      return box;
    }

    const createListener = ()=>{
      let box;
      const updateRate = 1/60; 
      window.addEventListener('deviceorientation', (event) => {
        const rotateDegrees = event.alpha;
        const frontToBack = event.beta - 40;
        const leftToRight = event.gamma;
        
        const px = Math.max(-1, Math.min(leftToRight / 30, 1));
        const py = Math.max(-1, Math.min(frontToBack / 30, 1));
        
        this.mouse.x = px;
        this.mouse.y = py;
        if(box){
          box.textContent = JSON.stringify({
            rotateDegrees: parseInt(rotateDegrees),
            leftToRight: parseInt(leftToRight),
            frontToBack: parseInt(frontToBack),
            px: px.toFixed(1),
            py: px.toFixed(1),
          });  
        }
      });
    }

    try{
      if (DeviceOrientationEvent?.requestPermission) {
        const clickHandler = ()=>{
          DeviceOrientationEvent.requestPermission().then(permissionState => {
            if (permissionState === 'granted') {
              createListener();
            }
          })
          .catch(console.error);
          window.removeEventListener('click', clickHandler);
        }
        window.addEventListener('click', clickHandler)
        
      } else {
        createListener()
      }
    } catch(err){
      console.log(err);
    }
  }
  private updateAcc(){
    // const x = Math.max(-1, Math.min(this.accParams.x / 30, 1)) * window.innerWidth; 
    // const y = Math.max(-1, Math.min(this.accParams.y / 30, 1)) * window.innerHeight; 
    // this.mouseInertia.x.update(x);
    // this.mouseInertia.y.update(y);


  }

  private render(){  
    this.clock.getDelta();
    this.onRender();
    this.fps = parseInt(this.clock.getDelta() * 10000);
    requestAnimationFrame( this.render );
  }

  private onRender(){
    this.updateInertiaByMouse();
    this.updateAcc(); 
    this.stairs.cameraUpdate();
    
    if(!this.orbitControls.enabled){
      this.copyCameraFromStairs();
      this.transformCameraByMouse();
    }

    
    
    this.updateCameraForMobile();
    this.camera.updateProjectionMatrix();
    
    this.person.onRender(this.clock); 
    this.text.onRender();
    this.stairs.onRender(this.clock);
    // this.renderer.render(this.scene, this.camera);
    this.postprocessing.onRender(this.clock);
    this.particles.onRender(this.clock);
    
    this.camera.updateProjectionMatrix();
    if(this.orbitControls && this.orbitControls.enabled){
      this.orbitControls.update();
    } 
    this.stats.update();
  }

  setupGui(){
    const gui = new dat.GUI({autoPlace: true});
    // this.person.setupGui(gui);
    this.stairs.setupGui(gui, this.orbitControls);
    this.lightGroup.setupGui(gui);
    this.postprocessing.setupGui(gui);
    this.particles.setupGui(gui);
    //MOUSE PARALLAX 
    const mouseParallaxFolder = gui.addFolder('Mouse Parallax');
    const setBoth = (name:string)=>(value:number)=>{
      this.mouseInertia.x[name] = value;
      this.mouseInertia.y[name] = value;
    }
    mouseParallaxFolder.add(this.mouseInertia.x,'acc',0,1).name('Acceleration').listen().onChange(setBoth('acc'))
    mouseParallaxFolder.add(this.mouseInertia.x, 'friction',0,1).name('Friction').listen().onChange(setBoth('friction'))
    mouseParallaxFolder.add(this.settings.parallax, 'limit', 0, 90, 0.1).listen()
    mouseParallaxFolder.add(this.settings.parallax, 'keepCameraTargetX').name('Keep Target X')
    mouseParallaxFolder.add(this.settings.parallax, 'keepCameraTargetY').name('Keep Target Y')
    
    
    
    const cameraFolder = gui.addFolder('Camera')
    cameraFolder.add(this.stairs.camera, 'zoom', 0.5, 3, 0.01).listen();
    cameraFolder.add(this.camera, 'filmGauge', 10, 50, 0.1).onChange(()=>this.camera.updateProjectionMatrix());
    cameraFolder.add(this.camera, 'filmOffset', -100, 100, 0.1).onChange(()=>this.camera.updateProjectionMatrix());
    cameraFolder.add(this.camera.position, 'z', -1000, 1000, 0.1).listen();
    cameraFolder.add(this.camera.position, 'y', -1000, 1000, 0.1).listen();

    const scrollFolder = gui.addFolder('Scroll settings');
    scrollFolder.add(scroll, 'inertiaAcceleration', 0, 1, 0.01).listen();
    scrollFolder.add(scroll, 'inertiaFriction', 0, 1, 0.01).listen();
    scrollFolder.add(scroll, 'scrollSpeed', 0, 10, 0.01).listen();
    scrollFolder.add(scroll, 'deltaLimit', 20, 300).listen();


    this.text.setupGui(gui);
    gui.add(this.settings, 'fogDensity', 0, 400, 1);
    gui.add(this.fog, 'near', 0, 3000, 1).listen();
    gui.add(this.fog, 'far', 0, 3000, 1).listen();
    gui.addColor({color: '#'+this.fog.color.getHexString()}, 'color').onChange((color)=>{
      this.fog.color.set(color)
      this.scene.background.set(color)
    });
    gui.add(this.camera.position, 'z',-1000, 1000).listen();
    gui.add(this.camera.position, 'x',-1000, 1000).listen();
    gui.add(this.camera, 'zoom',-1000, 1000).listen();
    gui.add(this.renderer, 'toneMapping', toneMappingOptions).onChange(value=>{
      console.log(this.renderer.toneMapping)
    });
    gui.add(this.renderer, 'toneMappingExposure', 0, 10, 0.01);

    const particlesController = gui.add({'Particles': true}, 'Particles').onChange(enabled=>{
      const container = document.querySelector('.particles')
      container.style.display = enabled ? 'block' : 'none';
      const video = container.querySelector('video');
      if(enabled){
        video.src = video.dataset.src;
        video.play()
      } else {
        video.src = null;
        video.pause();
      }
    })
    particlesController.setValue(false);
    gui.add(this.settings, 'scrollText');

    gui.add({"Pixel Ratio": window.devicePixelRatio}, 'Pixel Ratio', 0, 5, 0.01).onChange(value=>{
      this.renderer.setPixelRatio(value);
      this.postprocessing.updateSize();
    })

    gui.close();
    if(process.env.PRODUCTION_STATE === 'prod'){
      gui.destroy();
    }
  }
}
