#pragma glslify: blendNormal = require(glsl-blend/normal)
#pragma glslify: blendScreen = require(glsl-blend/screen)
#pragma glslify: blendAdd = require(glsl-blend/add)
uniform sampler2D textsTexture;
uniform sampler2D inputBuffer;
uniform sampler2D sceneTexture;
uniform sampler2D particlesTexture;
uniform vec3 backgroundColor;
uniform vec2 resolution;
uniform float u_colorFactor;
uniform vec3 blackColor;
uniform vec3 whiteColor;
uniform float snowOpacity;

varying vec2 vUv;

mat4 contrastMatrix( float contrast ){
	float t = ( 1.0 - contrast ) / 2.0;
  return mat4( contrast, 0., 0., 0.,
               0., contrast, 0., 0.,
               0., 0., contrast, 0.,
               t, t, t, 1. );
}

vec3 brightnessContrast(vec3 value, float brightness, float contrast){
    return (value - 0.5) * contrast + 0.5 + brightness;
}

vec3 greyscale(vec3 color, float factor){
  float grey = 0.21 * color.r + 0.71 * color.g + 0.07 * color.b;
  return vec3(color.r * factor + grey * (1.0 - factor), color.g * factor + grey * (1.0 - factor), color.b * factor + grey * (1.0 - factor));
}

float avg(vec3 v){
  return (v.r + v.g + v.b) / 3.;
}
float avg(vec4 v){
  return (v.r + v.g + v.b + v.a) / 4.; 
}

void main() {
  vec4 textsTexel = texture2D(textsTexture, vUv);
  vec4 sceneTexel = texture2D(sceneTexture, vUv);
  vec4 inputTexel = texture2D(inputBuffer, vUv);
  vec4 particlesTexel = texture2D(particlesTexture, vUv);
  vec3 color = sceneTexel.rgb;
  vec3 mergedColor;
  bool isBackground = abs(avg(backgroundColor) - avg(color)) < 0.1;
  if(textsTexel.a > 0.){
    // cause bgColor a bit difference with composer.frameBufferType = HalfFloatType
    if(isBackground){
      color = blackColor;
    } else {
      color = whiteColor;
    }
    color = color * textsTexel.a + inputTexel.rgb * (1. - textsTexel.a);
    mergedColor = color;
  } else {    
    mergedColor = inputTexel.rgb;
  }
  
  float backgroundParticlesCoef = isBackground && particlesTexel.a > 0. ? (1. + particlesTexel.a) : 1.;
  float particleOpacity = particlesTexel.a * snowOpacity;
  vec3 colorWithParticles = vec3(1.,1.,1.) / backgroundParticlesCoef * particleOpacity + mergedColor * (1. - particleOpacity); 
  gl_FragColor = vec4(colorWithParticles, 1.);
}