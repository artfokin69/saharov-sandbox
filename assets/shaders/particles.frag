#pragma glslify: cnoise2 = require(glsl-noise/classic/2d); 
uniform float uTime;
uniform float progress;
uniform sampler2D pointTextures[2];
uniform vec4 resolution;
uniform float pointsCount;
uniform float skewY;
uniform float skewX;
uniform float scaleY;
uniform float scaleX;
uniform float rotateX;
uniform float rotateY;
varying vec3 vPosition;
varying float vLifeTime;
varying float vSize;
varying float vSpeed;
varying float vMovementProgress;
varying float vShowDuration;
varying float vHideDuration;
varying vec2 vUv;

const float HALF_PI = 1.570796327;

mat2 skewUV(float skewX, float skewY) {
  return mat2(1., tan(skewX),
              tan(skewY), 1.);
}

mat2 scale(float x, float y){
    return mat2(x, 0.0,
                0.0, y);
}

float rand(vec2 co){
  return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}
vec4 getTexture(){
	int index = int(floor(0.5 + rand(gl_FragCoord.xy) * (pointsCount - 1.)));
	if(index == 0){
		return texture2D(pointTextures[0], vUv);
	} else if (index == 1){
		return texture2D(pointTextures[1], vUv);
	}
}

float getCameraOpacity(){
	float distanceToCamera = distance(cameraPosition, vPosition);
	float minVisibleDistance = 50.;
	float opacityDistance = 200.;
	float closestLimit = max(0.,(distanceToCamera - minVisibleDistance));	
	float cameraOpacity = min( closestLimit / opacityDistance, 1.);	
	return cameraOpacity;
}

void main() {
	vec4 texture = getTexture();
	float cameraOpacity = getCameraOpacity();
	
	if(texture.a < 0.3)
		discard;
	float hideParticleStartTirgger = 0.9;
	float hideParticleDuration = 0.1;
	float hideOpacityCoef = 1. - clamp(vMovementProgress - hideParticleStartTirgger, 0., hideParticleDuration) / hideParticleDuration;
	float showParticleStartTrigger = 0.;
	float showParticleDuration = 0.1;
	float showOpacityCoef = clamp(vMovementProgress - showParticleStartTrigger, 0., showParticleDuration) / showParticleDuration;
	texture.a *= hideOpacityCoef * showOpacityCoef * cameraOpacity;
	gl_FragColor = texture;
}