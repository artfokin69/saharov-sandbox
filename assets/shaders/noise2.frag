#define F1 float
#define F2 vec2
#define F3 vec3
#define F4 vec4
#define kMaxIterations 24

varying vec2 vUv;
uniform vec2 resolution;
uniform float time;
uniform sampler2D inputBuffer;

float rand(vec2 uv, float t) {
    return fract(sin(dot(uv, vec2(1225.6548, 321.8942))) * 4251.4865 + t);
}

vec4 mainImage() {
    vec2 ps = vec2(1.0) / resolution.xy;
    vec2 uv = vUv.xy / resolution.xy;
    
    float scale = 50.0;

    vec2 offset = (rand(uv, 1.) - 0.5) * 2.0 * scale / resolution.xy;
    
    vec3 noise = texture2D(inputBuffer, uv + offset).rgb;
    vec4 color = texture2D(inputBuffer, uv);
    
    float amount = 0.1;
    
    color.rgb = mix(color.rgb, noise, amount);
    return color;
}


void main(){
  gl_FragColor = mainImage();
}