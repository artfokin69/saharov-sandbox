// https://www.geeks3d.com/20141201/how-to-rotate-a-vertex-by-a-quaternion-in-glsl/
// Given a quaternion q, a position v, you need to do this to rotate it:
// vec3 temp = cross(q.xyz, v) + q.w * v;
// vec3 rotated = v + 2.0*cross(q.xyz, temp);

// precision highp float;
#pragma glslify: snoise3 = require(glsl-noise/simplex/3d) 
#pragma glslify: rotate = require(glsl-rotate/rotate)
#pragma glslify: rotateZ = require(glsl-rotate/rotateZ)
#pragma glslify: rotateY = require(glsl-rotate/rotateY)
#pragma glslify: rotateX = require(glsl-rotate/rotateX)
uniform float uSineTime;
uniform float uTime;
uniform float uParticlesOffsetX;
uniform float uParticlesOffsetZ;
uniform vec3 uCenter;


attribute vec3 offset;
attribute float scale;
attribute float speed;
attribute float delay;


varying vec3 vPosition;
varying vec2 vUv;
varying float vMovementProgress;

const float HALF_PI = 1.570796327;
const float maxY = 1000.;
const float minY = -500.;
const float totalShift = maxY - minY;

float rand(vec2 co){
  return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

vec3 applyQuaternionToVector( vec4 q, vec3 v ){
    return v + 2.0 * cross( q.xyz, cross( q.xyz, v ) + q.w * v );
}

vec3 shiftPosition(vec3 originPos, float progress, vec3 originCenter){
  vec3 pos = originPos;
  float shiftY = minY + totalShift * (1. - progress) - 100.;
  
  float noise = snoise3(pos * progress / 1000.);
  float shiftX = noise * 500.;
  float shiftZ = noise * 500.;
  vec3 shift = vec3(shiftX, shiftY, shiftZ);
  pos += shift;
  vec3 center = originCenter + shift;
  pos = rotate(pos, center, HALF_PI * 2. * noise);
  pos = rotateZ(pos, HALF_PI * 0.6);
  return pos;
}

float getProgress(float time){
  if(time < delay){
    return 0.;
  }
  float lifeTime = totalShift / speed / 30.;
  return mod(time - delay, lifeTime) / lifeTime;
}


void main(){
	vUv = uv;
  vMovementProgress = getProgress(uTime);
	vec3 particleShift = offset * vec3(uParticlesOffsetX, 1., uParticlesOffsetZ);
  vec3 particleScale = position * scale;
  vec3 startPosition = particleScale + particleShift;
  vec3 center = uCenter + particleShift;
 	vPosition = shiftPosition(startPosition, vMovementProgress, center); 
	// vec4 orientation = normalize( mix( orientationStart, orientationEnd, 0.5 ) );
	// vec3 vcV = cross( orientation.xyz, vPosition );
	// vPosition = vcV * ( size * orientation.w ) + ( cross( orientation.xyz, vcV ) * size + vPosition );

	gl_Position = projectionMatrix * modelViewMatrix * vec4( vPosition, 1.0 );

}