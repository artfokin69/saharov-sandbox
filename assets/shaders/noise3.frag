#pragma glslify: blendAdd = require(glsl-blend/add)
#pragma glslify: blendAverage = require(glsl-blend/average)
#pragma glslify: blendColorBurn = require(glsl-blend/color-burn)
#pragma glslify: blendColorDodge = require(glsl-blend/color-dodge)
#pragma glslify: blendDarken = require(glsl-blend/darken)
#pragma glslify: blendDifference = require(glsl-blend/difference)
#pragma glslify: blendExclusion = require(glsl-blend/exclusion)
#pragma glslify: blendGlow = require(glsl-blend/glow)
#pragma glslify: blendHardLight = require(glsl-blend/hard-light)
#pragma glslify: blendHardMix = require(glsl-blend/hard-mix)
#pragma glslify: blendLighten = require(glsl-blend/lighten)
#pragma glslify: blendLinearBurn = require(glsl-blend/linear-burn)
#pragma glslify: blendLinearDodge = require(glsl-blend/linear-dodge)
#pragma glslify: blendLinearLight = require(glsl-blend/linear-light)
#pragma glslify: blendMultiply = require(glsl-blend/multiply)
#pragma glslify: blendNegation = require(glsl-blend/negation)
#pragma glslify: blendNormal = require(glsl-blend/normal)
#pragma glslify: blendOverlay = require(glsl-blend/overlay)
#pragma glslify: blendPhoenix = require(glsl-blend/phoenix)
#pragma glslify: blendPinLight = require(glsl-blend/pin-light)
#pragma glslify: blendReflect = require(glsl-blend/reflect)
#pragma glslify: blendScreen = require(glsl-blend/screen)
#pragma glslify: blendSoftLight = require(glsl-blend/soft-light)
#pragma glslify: blendSubtract = require(glsl-blend/subtract)
#pragma glslify: blendVividLight = require(glsl-blend/vivid-light)
varying vec2 vUv;
uniform vec2 resolution;
uniform float time;
uniform float timeScale;
uniform float grainSize;
uniform sampler2D inputBuffer;
uniform float opacity;
uniform float grainBrightness;
uniform bool useBlendMode;
uniform vec3 backgroundColor;
uniform float BGgrainSize;
uniform float BGopacity;
uniform float BGgrainBrightness;
uniform float BGLightness;

vec3 rgb2hsl(vec3 color) {
 	vec3 hsl; // init to 0 to avoid warnings ? (and reverse if + remove first part)

 	float fmin = min(min(color.r, color.g), color.b); //Min. value of RGB
 	float fmax = max(max(color.r, color.g), color.b); //Max. value of RGB
 	float delta = fmax - fmin; //Delta RGB value

 	hsl.z = (fmax + fmin) / 2.0; // Luminance

 	if (delta == 0.0) //This is a gray, no chroma...
 	{
 		hsl.x = 0.0; // Hue
 		hsl.y = 0.0; // Saturation
 	} else //Chromatic data...
 	{
 		if (hsl.z < 0.5)
 			hsl.y = delta / (fmax + fmin); // Saturation
 		else
 			hsl.y = delta / (2.0 - fmax - fmin); // Saturation

 		float deltaR = (((fmax - color.r) / 6.0) + (delta / 2.0)) / delta;
 		float deltaG = (((fmax - color.g) / 6.0) + (delta / 2.0)) / delta;
 		float deltaB = (((fmax - color.b) / 6.0) + (delta / 2.0)) / delta;

 		if (color.r == fmax)
 			hsl.x = deltaB - deltaG; // Hue
 		else if (color.g == fmax)
 			hsl.x = (1.0 / 3.0) + deltaR - deltaB; // Hue
 		else if (color.b == fmax)
 			hsl.x = (2.0 / 3.0) + deltaG - deltaR; // Hue

 		if (hsl.x < 0.0)
 			hsl.x += 1.0; // Hue
 		else if (hsl.x > 1.0)
 			hsl.x -= 1.0; // Hue
 	}

 	return hsl;
 }

float avg(vec3 v){
  return (v.r + v.g + v.b) / 3.;
}
float avg(vec4 v){
  return (v.r + v.g + v.b + v.a) / 4.; 
}

vec4 getGrain(vec2 uv, float time, float timeScale, float size){
  float x = (uv.x + 4.0 ) * (uv.y + 4.0 ) * (time * timeScale);
  return vec4(mod((mod(x, 13.0) + 1.0) * (mod(x, 123.0) + 1.0), 0.01)-0.005) * size;
}

void mainImage( out vec4 fragColor, in vec2 uv ){
  float _grainSize;
  float _opacity;
  float _grainBrightness;
  vec4 textureColor = texture2D(inputBuffer, uv);

  vec4 lightGrain = getGrain(uv, time, timeScale, BGgrainSize);
  vec4 darkGrain = getGrain(uv, time, timeScale, grainSize);

  vec4 lightColor = textureColor * (BGLightness - lightGrain);

  vec4 newTextureColor = vec4(blendAdd(lightColor.rgb, darkGrain.rgb), 1.);
  fragColor.rgb = mix(textureColor.rgb, newTextureColor.rgb, opacity);
  fragColor.a = 1.;
}


void main(){
  vec4 color;
  mainImage(color, vUv);
  gl_FragColor = color;
}