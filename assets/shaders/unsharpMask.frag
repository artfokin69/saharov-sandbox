uniform sampler2D inputBuffer;
uniform vec2 resolution;
uniform float size;
varying vec2 vUv;

vec4 unsharpenMask (sampler2D tex, vec2 fragCoord)
{
    // Sharpen detection matrix [0,1,0],[1,-4,1],[0,1,0]
    // Colors
    vec4 up_left = texture2D(tex, (fragCoord + vec2(-1, 1)) / resolution.xy);
    vec4 up = texture2D(tex, (fragCoord + vec2(0, 1)) / resolution.xy);
    vec4 up_right = texture2D(tex, (fragCoord + vec2(1, 1)) / resolution.xy);
    vec4 left = texture2D(tex, (fragCoord + vec2(-1, 0)) / resolution.xy);
    vec4 center = texture2D(tex, fragCoord / resolution.xy);
    vec4 right = texture2D(tex, (fragCoord + vec2(1, 0)) / resolution.xy);
    vec4 down_left = texture2D(tex, (fragCoord + vec2(-1, -1)) / resolution.xy);
    vec4 down = texture2D(tex, (fragCoord + vec2(0, -1)) / resolution.xy);
    vec4 down_right = texture2D(tex, (fragCoord + vec2(1, -1)) / resolution.xy);
    
    vec4 blur = 0.0625*up_left + 0.125*up + 0.0625*up_right + 0.125*left + 0.25*center + 0.125*right + 0.0625*down_left + 0.125*down + 0.0625*down_right;
    
    // Return edge detection
    return center + (center - blur) * size;
}

void main(){
  gl_FragColor = unsharpenMask(inputBuffer, vUv * resolution.xy);
}